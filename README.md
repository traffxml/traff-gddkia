[![Release](https://jitpack.io/v/org.traffxml/traff-gddkia.svg)](https://jitpack.io/#org.traffxml/traff-gddkia)

Javadoc for `master` is at https://traffxml.gitlab.io/traff-gddkia/javadoc/master/.

Javadoc for `dev` is at https://traffxml.gitlab.io/traff-gddkia/javadoc/dev/.
