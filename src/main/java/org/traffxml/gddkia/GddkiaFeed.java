/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml-gddkia library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.gddkia;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.traff.TraffMessage;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

/**
 * Encapsulates a feed in GDDKiA format.
 */
public class GddkiaFeed {
	/**
	 * The logger for log output.
	 * 
	 * Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	static final Logger LOG = LoggerFactory.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());

	/**
	 * Timestamp reported in the feed.
	 */
	public final Date gen;

	/**
	 * Messages in the feed.
	 */
	public final GddkiaMessage[] utr;

	/**
	 * Parses a feed in GDDKiA XML format.
	 * 
	 * @param stream A stream containing the XML data
	 * @return A feed containing the data from the stream
	 */
	public static GddkiaFeed parseXml(InputStream stream) {
		SAXParserFactory spf = SAXParserFactory.newInstance();
		SAXParser sp;
		GddkiaContentHandler contentHandler = new GddkiaContentHandler();
		try {
			sp = spf.newSAXParser();
			XMLReader xr = sp.getXMLReader();
			xr.setContentHandler(contentHandler);
			xr.parse(new InputSource(stream));
		} catch (ParserConfigurationException e) {
			LOG.debug("{}", e);
		} catch (SAXException e) {
			LOG.debug("{}", e);
		} catch (IOException e) {
			LOG.debug("{}", e);
		}
		return contentHandler.feed;
	}

	/**
	 * Converts the feed to a list of TraFF messages.
	 */
	public List<TraffMessage> toTraff(String sourcePrefix) {
		return toTraff(sourcePrefix, null);
	}

	/**
	 * Converts the feed to a list of TraFF messages.
	 * 
	 * <p>This method allows previously received messages to be specified as an optional argument, in order for
	 * information from those messages to be used in the feed: The result will hold cancellation messages for
	 * all message IDs in {@code oldMessages} which start with {@code sourcePrefix} and are
	 * not matched by a message in the GDDKiA feed. If messages in the GDDKiA feed are matched by an item in
	 * {@code oldMessages}, the new message will inherit the receive time of the old one.
	 * 
	 * @param sourcePrefix Source prefix for message IDs
	 * @param oldMessages Previously received messages
	 */
	public List<TraffMessage> toTraff(String sourcePrefix, Collection<TraffMessage> oldMessages) {
		Map<String, TraffMessage> outMap = new HashMap<String, TraffMessage>();
		for (GddkiaMessage inMessage : utr) {
			TraffMessage outMessage = inMessage.toTraff(sourcePrefix, gen, oldMessages);
			if (outMessage != null)
				outMap.put(outMessage.id, outMessage);
		}
		/* Add cancellations for all unmatched old messages */
		if (oldMessages != null)
			for (TraffMessage oldMessage : oldMessages)
				if (oldMessage.id.startsWith(sourcePrefix + ":")
						&& !outMap.containsKey(oldMessage.id)) {
					TraffMessage.Builder builder = new TraffMessage.Builder();
					builder.setId(oldMessage.id);
					builder.setReceiveTime(oldMessage.receiveTime);
					builder.setUpdateTime(gen);
					Date expirationTime = oldMessage.expirationTime;
					if (expirationTime == null)
						expirationTime = oldMessage.endTime;
					builder.setExpirationTime(expirationTime);
					builder.setCancellation(true);
					outMap.put(oldMessage.id, builder.build());
				}
		List<TraffMessage> res = new ArrayList<TraffMessage>();
		res.addAll(outMap.values());
		return res;
	}

	public GddkiaFeed(Date gen, GddkiaMessage[] utr) {
		super();
		this.gen = gen;
		this.utr = utr;
	}

	/**
	 * Content handler for parsing XML feeds.
	 */
	private static class GddkiaContentHandler implements ContentHandler {
		/** Known child elements of {@code utr} */
		private static String[] CHILDREN_UTR = {"typ", "nr_drogi", "woj", "km", "dl", "geo_lat", "geo_long",
				"nazwa_odcinka", "data_powstania", "data_likwidacji", "objazd", "rodzaj", "skutki",
				"ogr_nosnosc", "ogr_nacisk", "ogr_skrajnia_pozioma", "ogr_skrajnia_pionowa", "ogr_szerokosc",
				"ogr_predkosc", "ruch_wahadlowy", "sygnalizacja_swietlna", "awaria_mostu",
				"ruch_2_kierunkowy", "droga_zamknieta", "czasy_oczekiwania"};

		/** Known child elements of {@code czas_oczekiwania}. */
		private static String[] CHILDREN_CZAS_OCZEKIWANIA = {"kierunek", "czas_od", "czas_do", "pn_pt", "so",
		"ni"};

		/** Pattern for time in hh:mm format (local time and duration). */
		private static Pattern COLON_PATTERN = Pattern.compile(":");

		/** Tag stack; new elements are added at the end of the list. */
		List<String> tagStack = new ArrayList<String>();

		/** Character data for the current tag. */
		String charData;

		/** The complete feed. */
		GddkiaFeed feed;

		/** The timestamp of the entire feed. */
		Date timestamp;

		/** Messages parsed so far. */
		List<GddkiaMessage> messages = new ArrayList<GddkiaMessage>();

		/** Builder for the message being parsed. */
		GddkiaMessage.Builder messageBuilder;

		/** Builder for the delay being parsed. */
		GddkiaDelay.Builder delayBuilder;

		/**
		 * Returns whether the current tag stack is a path to a known element.
		 */
		private boolean isTagStackValid() {
			switch (tagStack.size()) {
			case 0:
				return true;
			case 1:
				return tagStack.get(0).equals("utrudnienia");
			case 2:
				return tagStack.get(0).equals("utrudnienia") && tagStack.get(1).equals("utr");
			case 3:
				if (!(tagStack.get(0).equals("utrudnienia") && tagStack.get(1).equals("utr")))
					return false;
				for (String tag : CHILDREN_UTR)
					if (tag.equals(tagStack.get(2)))
						return true;
				return false;
			case 4:
				if (!(tagStack.get(0).equals("utrudnienia") && tagStack.get(1).equals("utr")))
					return false;
				if (tagStack.get(2).equals("rodzaj") && tagStack.get(3).equals("poz"))
					return true;
				if (tagStack.get(2).equals("czasy_oczekiwania") && tagStack.get(3).equals("czas_oczekiwania"))
					return true;
				return false;
			case 5:
				if (!(tagStack.get(0).equals("utrudnienia") && tagStack.get(1).equals("utr") &&
						tagStack.get(2).equals("czasy_oczekiwania") && tagStack.get(3).equals("czas_oczekiwania")))
					return false;
				for (String tag : CHILDREN_CZAS_OCZEKIWANIA)
					if (tag.equals(tagStack.get(4)))
						return true;
				return false;
			default:
				return false;
			}
		}

		/**
		 * Parses a timestamp in ISO 8601 format.
		 * 
		 * @param iso8601 The timestamp
		 * @return A date corresponding to the timestamp supplied
		 */
		private static Date parseDate(String iso8601) {
			/* use a local variable because SimpleDateFormat#parse() is not thread-safe */
			DateFormat format;
			try {
				format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX", Locale.ROOT);
			} catch (IllegalArgumentException e) {
				/*
				 * Older Android versions do not support X as a placeholder for the date format.
				 * On those versions, fall back to Z. Side effect is that the parser will insist on a time
				 * zone format of +hhmm, throwing an exception upon encountering +hh or +hh:mm.
				 */
				format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ROOT);
			}
			if (iso8601 != null)
				try {
					return format.parse(iso8601);
				} catch (ParseException e) {
					LOG.warn("Not a valid ISO8601 timestamp: " + iso8601 + ", ignoring");
				}
			return null;
		}

		/**
		 * Parses a string formatted as a duration.
		 * 
		 * <p>Durations can be in hh:mm[:ss[.sss]] format. Fractional seconds will be ignored.
		 * 
		 * <p>hh, mm and ss must be positive. mm and ss must be less than 60.
		 * 
		 * <p>If the duration string contains invalid characters, the result is undefined.
		 * 
		 * @param duration The duration string
		 * @return The duration which corresponds to the argument
		 */
		private static Duration parseDuration(String duration) {
			if (duration == null)
				return null;
			String[] elements = COLON_PATTERN.split(duration);
			try {
				if ((elements.length < 2) || (elements.length > 3))
					throw new ParseException("Incorrect number of elements", 0);
				int hours = Integer.valueOf(elements[0]);
				int minutes = Integer.valueOf(elements[1]);
				int seconds = 0;
				if (elements.length > 2)
					seconds = Integer.valueOf(elements[2]);
				if ((hours < 0) || (minutes < 0) || (minutes > 59) || (seconds < 0) || (seconds > 59))
					throw new ParseException("Invalid duration", 0);
				return Duration.ZERO.plusHours(hours).plusMinutes(minutes).plusSeconds(seconds);
			} catch (ParseException e) {
				LOG.warn("Not a valid duration: " + duration + ", ignoring");
			}
			return null;
		}

		/**
		 * Parses a string formatted as local time.
		 * 
		 * <p>Local time specifies no date and no time zone. It can be in hh:mm[:ss[.sss]] format. Fractional
		 * seconds will be ignored.
		 * 
		 * <p>hh, mm and ss must be positive. mm and ss must be less than 60, and hh must be no greater than 24.
		 * If hh is 24, mm and ss must be zero.
		 * 
		 * <p>If the duration string contains invalid characters, the result is undefined.
		 * 
		 * @param localTime The local time string
		 * @return The local time which corresponds to the argument
		 */
		private static LocalTime parseLocalTime(String localTime) {
			if (localTime == null)
				return null;
			String[] elements = COLON_PATTERN.split(localTime);
			try {
				if ((elements.length < 2) || (elements.length > 3))
					throw new ParseException("Incorrect number of elements", 0);
				int hours = Integer.valueOf(elements[0]);
				int minutes = Integer.valueOf(elements[1]);
				int seconds = 0;
				if (elements.length > 2)
					seconds = Integer.valueOf(elements[2]);
				if ((hours < 0) || (hours > 24) || (minutes < 0) || (minutes > 59) || (seconds < 0) || (seconds > 59)
						|| ((hours == 24) && (minutes + seconds > 0)))
					throw new ParseException("Invalid time", 0);
				return LocalTime.MIDNIGHT.plusHours(hours).plusMinutes(minutes).plusSeconds(seconds);
			} catch (ParseException e) {
				LOG.warn("Not a valid local time: " + localTime + ", ignoring");
			}
			return null;
		}

		@Override
		public void characters(char[] ch, int start, int length) throws SAXException {
			// character data (element body)
			if (!isTagStackValid())
				return;
			charData = charData + String.copyValueOf(ch, start, length);
		}

		@Override
		public void endDocument() throws SAXException {
			// NOP
		}

		@Override
		public void endElement(String uri, String localName, String qName) throws SAXException {
			// end element, qName is the name
			if (tagStack.size() > 0) {
				if (!tagStack.get(tagStack.size() - 1).equals(qName))
					throw new IllegalStateException("End of XML element which does not match top of tag stack");
			} else
				throw new IllegalStateException("End of XML element but tag stack is empty");
			if (!charData.isEmpty()) {
				if (qName.equals("typ"))
					messageBuilder.setTyp(charData);
				else if (qName.equals("nr_drogi"))
					messageBuilder.setNrDrogi(charData);
				else if (qName.equals("woj"))
					messageBuilder.setWoj(charData);
				else if (qName.equals("km"))
					messageBuilder.setKm(Float.valueOf(charData));
				else if (qName.equals("dl"))
					messageBuilder.setDl(Float.valueOf(charData));
				else if (qName.equals("geo_lat"))
					messageBuilder.setGeoLat(Float.valueOf(charData));
				else if (qName.equals("geo_long"))
					messageBuilder.setGeoLong(Float.valueOf(charData));
				else if (qName.equals("nazwa_odcinka"))
					messageBuilder.setNazwaOdcinka(charData);
				else if (qName.equals("data_powstania"))
					messageBuilder.setDataPowstania(parseDate(charData));
				else if (qName.equals("data_likwidacji"))
					messageBuilder.setDataLikwidacji(parseDate(charData));
				else if (qName.equals("objazd"))
					messageBuilder.setObjazd(charData);
				else if (qName.equals("poz"))
					messageBuilder.addRodzaj(charData);
				else if (qName.equals("skutki"))
					messageBuilder.setSkutki(charData);
				else if (qName.equals("ogr_nosnosc"))
					messageBuilder.setOgrNosnosc(Float.valueOf(charData));
				else if (qName.equals("ogr_nacisk"))
					messageBuilder.setOgrNacisk(Float.valueOf(charData));
				else if (qName.equals("ogr_skrajnia_pozioma"))
					messageBuilder.setOgrSkrajniaPozioma(Float.valueOf(charData));
				else if (qName.equals("ogr_skrajnia_pionowa"))
					messageBuilder.setOgrSkrajniaPionowa(Float.valueOf(charData));
				else if (qName.equals("ogr_szerokosc"))
					messageBuilder.setOgrSzerokosc(Float.valueOf(charData));
				else if (qName.equals("ogr_predkosc"))
					messageBuilder.setOgrPredkosc(Integer.valueOf(charData));
				else if (qName.equals("ruch_wahadlowy"))
					messageBuilder.setRuchWahadlowy(Boolean.valueOf(charData));
				else if (qName.equals("sygnalizacja_swietlna"))
					messageBuilder.setSygnalizacjaSwietlna(Boolean.valueOf(charData));
				else if (qName.equals("awaria_mostu"))
					messageBuilder.setAwariaMostu(Boolean.valueOf(charData));
				else if (qName.equals("ruch_2_kierunkowy"))
					messageBuilder.setRuch2Kierunkowy(Boolean.valueOf(charData));
				else if (qName.equals("droga_zamknieta"))
					messageBuilder.setDrogaZamknieta(Boolean.valueOf(charData));
				else if (qName.equals("kierunek"))
					delayBuilder.setKierunek(charData);
				else if (qName.equals("czas_od"))
					delayBuilder.setCzasOd(parseLocalTime(charData));
				else if (qName.equals("czas_do"))
					delayBuilder.setCzasDo(parseLocalTime(charData));
				else if (qName.equals("pn_pt"))
					delayBuilder.setPnPt(parseDuration(charData));
				else if (qName.equals("so"))
					delayBuilder.setSo(parseDuration(charData));
				else if (qName.equals("ni"))
					delayBuilder.setNi(parseDuration(charData));
			}
			tagStack.remove(tagStack.size() - 1);
			if (qName.equals("utrudnienia"))
				feed = new GddkiaFeed(timestamp, messages.toArray(new GddkiaMessage[0]));
			else if (qName.equals("utr")) {
				messages.add(messageBuilder.build());
				messageBuilder = null;
			} else if (qName.equals("czas_oczekiwania")) {
				messageBuilder.addCzasOczekiwania(delayBuilder.build());
				delayBuilder = null;
			}
		}

		@Override
		public void endPrefixMapping(String prefix) throws SAXException {
			// NOP
		}

		@Override
		public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
			// NOP
		}

		@Override
		public void processingInstruction(String target, String data) throws SAXException {
			// NOP
		}

		@Override
		public void setDocumentLocator(Locator locator) {
			// NOP
		}

		@Override
		public void skippedEntity(String name) throws SAXException {
			// NOP
		}

		@Override
		public void startDocument() throws SAXException {
			// NOP
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
			// start of element, qName is the name
			tagStack.add(qName);
			/* 
			 * Resetting charData when entering a new element, appending on encountering character data and
			 * processing everything upon leaving that element works only as long as none of the elements
			 * with character data have any children. If that happens, we have to convert charData into a
			 * stack similar to tagStack.
			 */
			charData = "";
			if (!isTagStackValid())
				return;
			if (qName.equals("utrudnienia"))
				timestamp = parseDate(atts.getValue("gen"));
			else if (qName.equals("utr"))
				messageBuilder = new GddkiaMessage.Builder();
			else if (qName.equals("czas_oczekiwania"))
				delayBuilder = new GddkiaDelay.Builder();
		}

		@Override
		public void startPrefixMapping(String prefix, String uri) throws SAXException {
			// NOP
		}

	}
}
