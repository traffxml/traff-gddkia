/*
 * Copyright © 2019–2020 traffxml.org.
 * 
 * This file is part of the traffxml-gddkia library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.gddkia;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.input.BOMInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.traffxml.lib.milestone.Distance;
import org.traffxml.lib.milestone.Junction;
import org.traffxml.lib.milestone.JunctionList;
import org.traffxml.lib.milestone.JunctionUtils;
import org.traffxml.lib.milestone.Milestone;
import org.traffxml.lib.milestone.MilestoneList;
import org.traffxml.lib.milestone.MilestoneUtils;
import org.traffxml.traff.BoundingBox;
import org.traffxml.traff.DimensionQuantifier;
import org.traffxml.traff.LatLon;
import org.traffxml.traff.TraffEvent;
import org.traffxml.traff.TraffLocation;
import org.traffxml.traff.TraffMessage;
import org.traffxml.traff.TraffSupplementaryInfo;
import org.traffxml.traff.WeightQuantifier;
import org.traffxml.traff.TraffLocation.Directionality;
import org.traffxml.traff.TraffLocation.Fuzziness;
import org.traffxml.traff.TraffLocation.RoadClass;

/**
 * Encapsulates a message in GDDKiA format, equivalent to an {@code utr} element in a feed.
 *
 * <p>New messages are generated using a {@link Builder}.
 * 
 * <p>{@link #geoLat} and {@link #geoLong} can be set to supply a coordinate pair, but they are not always set.
 * They appear to be placed in the vicinity of the point referenced by {@link #km}, although accuracy varies
 * greatly. Some are spot on, some can be several hundred meters from the road they refer to, some have been
 * spotted on the road but up to 5 km from the actual location. The most extreme case observed was a location
 * near Olsztynek, with {@link #geoLat} and {@link #geoLong} referring to a location on the German border
 * near Szczecin.
 * 
 * <p>{@link #nrDrogi} is the road ref as supplied by GDDKiA. This is either the road ref or a section ref, which
 * is the road ref with an extra letter appended. Section refs are used for disambiguation when the road has
 * duplicate kilometric points. Section refs are unique across different road classes with the same number,
 * but a section can be composed of different road classes if kilometric point numbering is continuous (e.g.
 * A8e/S8e or S16i/16i). Stretches of the road whose kilometric points refer to the “original” zero kilometer
 * of the road (or the first one constructed, or the one with the longest continuous numbering) mostly do not
 * have a section ref and the road ref is used. Sometimes, however, these sections do have a section ref, and
 * either the road ref or the section ref may be used (examples being the S3a or S52b). The road ref can be
 * obtained with {@link #extractRoadRef()}.
 * 
 * <p>GDDKiA appears to follow certain constraints in their locations and split messages in two if the entire
 * extent of the situation cannot be expressed without violating them. For one, a location can never cross a
 * voivodeship border. Furthermore, both end points must be relative to the same zero point, thus
 * {@code km + dl} will always result in a valid kilometric point. Note, however, that one section of a road
 * can interrupt another (e.g. S99a can run from km 0 to 29.1, then S99h starts at km 0 and continues up to
 * km 11.2, after which S99a resumes at km 39.7). Locations can extend across the entire “inner” section,
 * i.e. it is legal for a location to start on S99a at km 28.6 and continue for 13.7 km, thus enclosing S99h
 * in its entirety.
 */
public class GddkiaMessage {
	/**
	 * The logger for log output.
	 * 
	 * <p>Where messages will be logged depends on the slf4j binding supplied at runtime.
	 */
	static final Logger LOG = LoggerFactory.getLogger(Thread.currentThread().getStackTrace()[2].getClassName());

	public static enum Direction { FORWARD, BACKWARD };

	/**
	 * CSV attribute name for the road destination.
	 */
	private static final String ATTR_DESTINATION = "destination";

	/**
	 * CSV attribute name for the road origin.
	 */
	private static final String ATTR_ORIGIN = "origin";

	/**
	 * CSV attribute name for the “urban” flag.
	 */
	private static final String ATTR_ROAD_IS_URBAN = "road_is_urban";

	/**
	 * CSV attribute name for the road name.
	 */
	private static final String ATTR_ROAD_NAME = "road_name";

	/**
	 * CSV attribute name for the town.
	 */
	private static final String ATTR_TOWN = "town";

	/**
	 * Maximum distance for clustering milestones.
	 */
	private static final float CLUSTER_DISTANCE_KM = 0.075f;

	/**
	 * Tolerance for milestone resolution (distance by which the affected area can extend outside its actual location), in km.
	 */
	private static final float DIST_TOLERANCE_KM = 2.0f;

	/**
	 * Tolerance when comparing distances for consistency with the reference coordinates, in km.
	 */
	private static final float GEO_CONSISTENCY_TOLERANCE_KM = 5.0f;

	/**
	 * Maximum tolerance for interpolation.
	 */
	private static final float INTERPOLATION_TOLERANCE_KM = 0.1f;

	/**
	 * Separators used in junction names to separate town and district names
	 */
	static final String[] JUNCTION_COMMON_SUFFIX_SEPARATORS = {" ", "-"};

	/**
	 * Common suffixes used in junction names after the town name.
	 * 
	 * <p>This is used to associate a town with coordinates by looking for junctions serving that town.
	 */
	static final String[] JUNCTION_COMMON_SUFFIXES = {"Centrum", "centrum", "Północ", "północ", "Wschód",
			"wschód", "Południe", "południe", "Zachód", "zachód"};

	/**
	 * Maximum distance between two junctions, in km.
	 */
	private static final float JUNCTION_MAX_DISTANCE = 20.0f;

	/*
	 * Important:
	 * - leading spaces and trailing space or punctuation (to avoid matching partial words)
	 * - if one keyword is a substring of another, put the substring after the containing string
	 */
	/**
	 * Prefixes to strip from junction names.
	 */
	static final String[] JUNCTION_PREFIX = {"w.", "W.", "węzeł ", "Węzeł "};

	/**
	 * Pattern to split a string at a junction separator.
	 * 
	 * <p>This contains various kinds of “fool’s dash” (hyphen enclosed in spaces, hyphen with a space on one
	 * side, double hyphen) or a hyphen (as a substitute for a dash) followed by an unambiguous keyword.
	 */
	static final Pattern JUNCTION_SEPARATOR_PATTERN =
			Pattern.compile("( - )|(- )|( -)|(--)|(-[wW]((\\.)|(ęzeł)))|(do węzła)");

	/**
	 * Common junction name suffixes by which we can recognize the end of a junction name.
	 */
	// TODO complete all common abbreviations
	static final String[] JUNCTION_SUFFIX = {"Północ", " północ", "Wschód", " wschód", "Wsch", "Południe",
			" południe", "Płd.", " płd.", "Zachód", " zachód"};

	/**
	 * Keywords denoting parts of a string that follow junctions (other than destinations).
	 */
	static final String[] KEYWORDS_AFTER_JUNCTION = {" jezdnia lewa ", "(jezdnia lewa ", " jezdnia prawa ",
			"(jezdnia prawa ", " na trasie "};

	/**
	 * Keywords denoting a destination.
	 */
	static final String[] KEYWORDS_DESTINATION = {" kier.", " kierunek ", " w kierunku na", " w kierunku "};

	/**
	 * Keywords denoting a destination, suitable for counting (no duplicate matches).
	 */
	static final String[] KEYWORDS_DESTINATION_COUNT = {" kier.", " kierunek ", " w kierunku "};

	/**
	 * Keywords denoting directionality.
	 */
	static final String[] KEYWORDS_DIRECTIONALITY = {"Jezdnia lewa ", " jezdnia lewa ", "(jezdnia lewa ",
			" na jezdni lewej ", "Jezdnia prawa ", " jezdnia prawa ", "(jezdnia prawa ", " na jezdni prawej ",
			" kier.", " kierunek ", " w kierunku na", " w kierunku "};

	/**
	 * Keywords denoting forward directionality.
	 */
	static final String[] KEYWORDS_DIRECTIONALITY_FORWARD = {"Jezdnia prawa ", " jezdnia prawa ", "(jezdnia prawa ",
			" na jezdni prawej "};

	/**
	 * Keywords denoting backward directionality.
	 */
	static final String[] KEYWORDS_DIRECTIONALITY_BACKWARD = {"Jezdnia lewa ", " jezdnia lewa ", "(jezdnia lewa ",
			" na jezdni lewej "};

	/**
	 * Keywords denoting junction names.
	 */
	static final String[] KEYWORDS_JUNCTION = {" m.", " MOP ", " miejscowość ", " msc.", " PPO ",
			" w.", " węzeł ", " Węzeł "};

	/**
	 * Tolerance to establish consistency or redundancy for elimination, in km.
	 */
	/*
	 * FIXME this might not be sufficient, DK50 has 500 m of dupes between km 179–180 (Góra Kalwaria bypass).
	 */
	private static final float REDUNDANCY_TOLERANCE_KM = 0.3f;

	/**
	 * Comparator for strings with numbers (12A).
	 */
	private static final NumberStringComparator REF_COMPARATOR = new NumberStringComparator();

	/**
	 * Field name for road ref.
	 */
	private static final String ROAD_REF_NAME="road_ref";

	/**
	 * Pattern to parse OSM-style lists.
	 */
	static final Pattern SEMICOLON_PATTERN = Pattern.compile(";");

	/**
	 * Pattern to parse CSV lines (with tabs as separators).
	 */
	static final Pattern TAB_PATTERN = Pattern.compile("\t");

	/**
	 * Map of road attributes.
	 */
	static Map<String, Map<String, String>> roadAttributes = new HashMap<String, Map<String, String>>();

	/**
	 * Map of junctions, indexed by road reference and junction name.
	 */
	static JunctionList junctionsByName;

	/**
	 * Map of milestones.
	 */
	static MilestoneList milestones;

	/**
	 * Map of entries in rozaj/poz and skutki and their corresponding events.
	 * 
	 * <p>The key is the event code as it appears in the feed (note that skutki can have textual descriptions
	 * instead of codes as keys).
	 */
	static Map<String, EventMapping> events = new HashMap<String, EventMapping>();

	public final String typ;
	public final String nrDrogi;
	public final String woj;

	/**
	 * Kilometric point for the location.
	 * 
	 * <p>If {@code dl > 0}, it is the end point with the lower distance.
	 */
	public final float km;

	/**
	 * Length of the location.
	 * 
	 * <p>The end point with the higher distance is at {@code km + dl}. This appears to always refer to a valid
	 * kilometric point, i.e. locations never extend beyond a milepost equation. Where a situation extends
	 * beyond a milepost equation, multiple messages are created.
	 */
	public final float dl;

	/**
	 * Reference latitude.
	 */
	public final Float geoLat;

	/**
	 * Reference longitude.
	 */
	public final Float geoLong;

	public final String nazwaOdcinka;
	public final Date dataPowstania;
	public final Date dataLikwidacji;
	public final String objazd;
	// objazdMapy is not represented
	public final String[] rodzaj;
	public final String skutki;
	public final Float ogrNosnosc;
	public final Float ogrNacisk;
	public final Float ogrSkrajniaPozioma;
	public final Float ogrSkrajniaPionowa;
	public final Float ogrSzerokosc;
	public final Integer ogrPredkosc;
	public final Boolean ruchWahadlowy;
	public final Boolean sygnalizacjaSwietlna;
	public final Boolean awariaMostu;
	public final Boolean ruch2Kierunkowy;
	public final Boolean drogaZamknieta;
	public final GddkiaDelay[] czasyOczekiwania;

	/* cached data */
	private String destination = null;
	private Junction[] enclosingJunctions = null;
	private Junction[] endpointJunctions = null;
	private Map <TraffLocation.Point.Role, Point> endpoints = null;
	private String[] rawJunctions = null;
	private Set<Junction>[] resolvedJunctions;
	private Map<String, Set<Junction>> roadJunctions = null;

	static {
		/* Read milestone data set */
		try {
			milestones = new MilestoneList(GddkiaMessage.class.getResourceAsStream("milestones.csv"),
					'\t', ';', "@lat", "@lon", "ref", "distance", Distance.UNIT_KM);
		} catch (IOException e) {
			LOG.debug("{}", e);
		}

		/* Read junction data set */
		try {
			junctionsByName = new JunctionList(GddkiaMessage.class.getResourceAsStream("junctions.csv"),
					'\t', ';', "@lat", "@lon", "road_ref", "ref", "name", "milestone", Distance.UNIT_KM);
		} catch (IOException e) {
			LOG.debug("{}", e);
		}

		/* Read event data set */
		try {
			BufferedReader br = openTextFile("events.csv");
			String line = br.readLine(); // drop the first line
			while ((line = br.readLine()) != null) {
				String gddkiaEvent;
				String traffEvent;
				String traffSi = null;
				String[] fields = TAB_PATTERN.split(line);
				if (fields.length < 2)
					continue;
				gddkiaEvent = fields[0];
				traffEvent = fields[1];
				if (fields.length >= 3)
					traffSi = fields[2];
				if (((traffEvent == null) || traffEvent.isEmpty()) && ((traffSi == null) || traffSi.isEmpty()))
					continue;
				events.put(gddkiaEvent, new EventMapping(gddkiaEvent, traffEvent, traffSi));
			}
		} catch (IOException e) {
			LOG.debug("{}", e);
		}

		/* Read road attribute data set */
		try {
			// roadAttributes
			InputStreamReader reader = new InputStreamReader(new BOMInputStream(GddkiaMessage.class.getResourceAsStream("roads.csv")));
			Iterable<CSVRecord> records;
			records = CSVFormat.DEFAULT.withDelimiter('\t').withFirstRecordAsHeader().parse(reader);
			for (CSVRecord record : records)
				try {
					String roadRef = record.get(ROAD_REF_NAME);
					if ((roadRef == null) || (roadRef.trim().isEmpty()))
						continue;
					roadRef = roadRef.trim();
					Map<String, String> attrs = record.toMap();
					attrs.remove(ROAD_REF_NAME);
					for (Iterator<Map.Entry<String, String>> it = attrs.entrySet().iterator(); it.hasNext(); ) {
						Map.Entry<String, String> entry = it.next();
						if ((entry.getValue() == null) || (entry.getValue().trim().isEmpty()))
							it.remove();
					}
					if (!attrs.isEmpty())
						roadAttributes.put(roadRef, attrs);
				} catch (Exception e2) {
					continue;
				}
		} catch (IOException e) {
			LOG.debug("{}", e);
		}
	}

	/**
	 * Retrieves endpoint coordinates from a message by using junctions.
	 * 
	 * <p>This method is intended as a fallback if endpoints cannot be obtained using milestones.
	 * 
	 * <p>If {@link #extractResolvedJunctions()} does not return two non-empty sets, or the order of the two
	 * sets on the road cannot be established, no endpoints can be determined.
	 * 
	 * <p>If endpoint extraction was successful, the endpoints are stored in {@link #endpoints}. The
	 * {@link #endpoints} member must be initialized with an empty map prior to calling this method.
	 * If no suitable endpoints are found, {@link #endpoints} will be an empty map (however, non-null).
	 * Otherwise, it will contain a map of endpoints and their roles.
	 * 
	 * <p>Junction names are stored in {@link #endpointJunctions} and can be retrieved with
	 * {@link #extractEndpointJunctions()} (which in this case will not perform any logic apart from
	 * retrieving the pre-cached values) after this method returns.
	 */
	private void extractEndpointsUsingJunctions() {
		extractResolvedJunctions();
		if (resolvedJunctions[0].isEmpty() || resolvedJunctions[1].isEmpty()) {
			LOG.debug("Could not determine junctions for {} @ {} + {} on at least one side",
					nrDrogi, km, dl);
			return;
		}
		Float[] kmMin = {null, null};
		Float[] kmMax = {null, null};
		String[] refMin = {null, null};
		String[] refMax = {null, null};
		String[] name = {null, null};
		for (int i = 0; i < 2; i++)
			for (Junction junction : resolvedJunctions[i]) {
				if (junction.name != null) {
					if (name[i] == null)
						name[i] = junction.name;
					else if (!name[i].matches(junction.name)) {
						LOG.debug("Could not determine junctions for {} @ {} + {}: ambiguous junction names",
								nrDrogi, km, dl);
						return;
					}
				}
				if (junction.distance != null ) {
					if ((kmMin[i] == null) || (junction.distance.asUnit(Distance.UNIT_KM) < kmMin[i]))
						kmMin[i] = junction.distance.asUnit(Distance.UNIT_KM);
					if ((kmMax[i] == null) || (junction.distance.asUnit(Distance.UNIT_KM) > kmMax[i]))
						kmMax[i] = junction.distance.asUnit(Distance.UNIT_KM);
				}
				if (junction.ref != null) {
					if ((refMin[i] == null) || (REF_COMPARATOR.compare(junction.ref, refMin[i]) < 0))
						refMin[i] = junction.ref;
					if ((refMax[i] == null) || (REF_COMPARATOR.compare(junction.ref, refMax[i]) > 0))
						refMax[i] = junction.ref;
				}
			}
		/*
		 * Junction set order (to be interpreted similar to Comparator output):
		 * -1: in order (0 before 1)
		 * 1: reversed order (1 before 0)
		 * 0: no order could be established
		 */
		int order = 0;
		if ((kmMin[0] != null) && (kmMin[1] != null)) {
			if (kmMax[0] < kmMin[1])
				order -= 1;
			else if (kmMin[0] > kmMax[1])
				order += 1;
		}
		if ((refMin[0] != null) && (refMin[1] != null)) {
			if (REF_COMPARATOR.compare(refMax[0], refMin[1]) < 0)
				order -= 1;
			else if (REF_COMPARATOR.compare(refMin[0], refMax[1]) > 0)
				order += 1;
		}
		if (order == 0) {
			LOG.debug("Could not determine junctions for {} @ {} + {}: order could not be established",
					nrDrogi, km, dl);
			return;
		}

		/* here we have exactly one junction per side, with an unambiguous name (but possibly multiple points) */
		endpointJunctions = new Junction[]{null, null};
		/* mapping between from/to and indices in resolvedJunctions (works both ways) */
		int[] idx = {
				((order < 0) ? 0 : 1),
				((order < 0) ? 1 : 0)
		};
		TraffLocation.Point.Role [] roles = { TraffLocation.Point.Role.FROM, TraffLocation.Point.Role.TO };
		for (int i = 0; i < 2; i++) {
			endpointJunctions[i] = JunctionUtils.mergeJunctions(resolvedJunctions[idx[i]]);
			// FIXME do we need actualDistance at all? can we make it nullable?
			endpoints.put(roles[i], new Point(km + i * dl, endpointJunctions[i].geo, km + i * dl));
		}
	}

	/**
	 * Returns the junctions enclosing the location, verified against the map and matched up with the respective junctions.
	 * 
	 * <p>Returned junctions are unsorted.
	 */
	/*
	 * TODO handle different spellings:
	 * Szczecin-Kijewo vs. Szczecin Kijewo
	 * Szczecin vs. SZCZECIN
	 * 
	 */
	private Set<Junction>[] extractResolvedJunctions() {
		/* populate the cache */
		extractJunctions();
		if (roadJunctions == null)
			roadJunctions = junctionsByName.getByName(extractRoadRef());

		if (resolvedJunctions != null)
			return resolvedJunctions;

		resolvedJunctions = new Set[2];
		resolvedJunctions[0] = new HashSet<Junction>();
		resolvedJunctions[1] = new HashSet<Junction>();

		if (roadJunctions == null)
			return resolvedJunctions;

		for (int i = 0; i < 2; i++) {
			if (rawJunctions[i] == null)
				continue;
			if (roadJunctions.containsKey(rawJunctions[i]))
				resolvedJunctions[i].addAll(roadJunctions.get(rawJunctions[i]));
			for (String suffix : JUNCTION_SUFFIX) {
				String junctionName = rawJunctions[i] + " " + suffix.trim();
				if (roadJunctions.containsKey(junctionName))
					resolvedJunctions[i].addAll(roadJunctions.get(junctionName));
			}
		}

		return resolvedJunctions;
	}

	/**
	 * Gets the approximate coordinates of a destination on a road.
	 * 
	 * <p>The coordinates are determined by examining all junctions whose names match the town name, optionally
	 * with a standard suffix (such as Centrum, Północ, Wschód, Południe and Zachód), and averaging over
	 * these values.
	 * 
	 * @param ref The road reference
	 * @param name The junction name
	 * @return The average of all coordinate pairs, or null if no entries were found
	 */
	private static LatLon getDestinationCoords(String ref, String name) {
		float latSum = 0;
		float lonSum = 0;
		float count = 0;
		Set<String> names = new HashSet<String>();
		names.add(name);
		for (String suffix : JUNCTION_COMMON_SUFFIXES)
			for (String separator : JUNCTION_COMMON_SUFFIX_SEPARATORS)
				names.add(name + separator + suffix);
		for (String junctionName : names) {
			LatLon point = junctionsByName.getCoordsByName(ref, junctionName);
			if (point != null) {
				latSum += point.lat;
				lonSum += point.lon;
				count++;
			}
		}
		if (count == 0)
			return null;
		try {
			return new LatLon(latSum / count, lonSum / count);
		} catch (IllegalArgumentException e) {
			return null;
		}
	}

	/**
	 * Gets the approximate kilometric point of a destination on a road.
	 * 
	 * <p>The kilometric point is determined by examining all junctions whose names match the town name,
	 * optionally with a standard suffix (such as Centrum, Północ, Wschód, Południe and Zachód), and
	 * averaging over these values.
	 * 
	 * @param ref The road reference
	 * @param name The junction name
	 * @return The average of all kilometric points, or null if no entries were found
	 */
	private static Distance getDestinationDistance(String ref, String name) {
		float sum = 0;
		float count = 0;
		Set<String> names = new HashSet<String>();
		names.add(name);
		for (String suffix : JUNCTION_COMMON_SUFFIXES)
			for (String separator : JUNCTION_COMMON_SUFFIX_SEPARATORS)
				names.add(name + separator + suffix);
		for (String junctionName : names) {
			Distance distance = junctionsByName.getDistanceByName(ref, junctionName);
			if (distance != null) {
				sum += distance.asUnit(Distance.UNIT_M);
				count++;
			}
		}
		if (count == 0)
			return null;
		return new Distance(sum / count, Distance.UNIT_M);
	}

	/**
	 * Returns the earlier of two dates.
	 * 
	 * <p>If one argument is null, the other is returned. If both are null, null is returned.
	 * 
	 * @param lhs The first date
	 * @param rhs The second date
	 * @return The earlier of the two dates, or the only non-null date, or null if both dates are null
	 */
	private static Date earlier(Date lhs, Date rhs) {
		if (lhs == null)
			return rhs;
		if (rhs == null)
			return lhs;
		if (lhs.before(rhs))
			return lhs;
		else
			return rhs;
	}

	/**
	 * Returns the later of two dates.
	 * 
	 * <p>If one argument is null, the other is returned. If both are null, null is returned.
	 * 
	 * @param lhs The first date
	 * @param rhs The second date
	 * @return The later of the two dates, or the only non-null date, or null if both dates are null
	 */
	private static Date later(Date lhs, Date rhs) {
		if (lhs == null)
			return rhs;
		if (rhs == null)
			return lhs;
		if (lhs.after(rhs))
			return lhs;
		else
			return rhs;
	}

	/**
	 * Opens a text file contained in a Java resource.
	 * 
	 * @param name The local filename
	 * @return
	 * @throws IOException
	 */
	static BufferedReader openTextFile(String name) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(
				GddkiaMessage.class.getResourceAsStream(name)));
		// skip first line
		br.readLine();
		return br;
	}

	/**
	 * Formats a kilometric point in km+mmm format.
	 */
	public static String formatKm(float km) {
		String ret = String.format("%.3f", km);
		ret = ret.replace('.', '+');
		return ret;
	}

	private GddkiaMessage(String typ, String nrDrogi, String woj, float km, float dl, Float geoLat,
			Float geoLong, String nazwaOdcinka, Date dataPowstania, Date dataLikwidacji, String objazd,
			String[] rodzaj, String skutki, Float ogrNosnosc, Float ogrNacisk, Float ogrSkrajniaPozioma,
			Float ogrSkrajniaPionowa, Float ogrSzerokosc, Integer ogrPredkosc, Boolean ruchWahadlowy,
			Boolean sygnalizacjaSwietlna, Boolean awariaMostu, Boolean ruch2Kierunkowy,
			Boolean drogaZamknieta, GddkiaDelay[] czasyOczekiwania) {
		super();
		this.typ = typ;
		this.nrDrogi = nrDrogi;
		this.woj = woj;
		this.km = km;
		this.dl = dl;
		this.geoLat = geoLat;
		this.geoLong = geoLong;
		this.nazwaOdcinka = nazwaOdcinka;
		this.dataPowstania = dataPowstania;
		this.dataLikwidacji = dataLikwidacji;
		this.objazd = objazd;
		this.rodzaj = rodzaj;
		this.skutki = skutki;
		this.ogrNosnosc = ogrNosnosc;
		this.ogrNacisk = ogrNacisk;
		this.ogrSkrajniaPozioma = ogrSkrajniaPozioma;
		this.ogrSkrajniaPionowa = ogrSkrajniaPionowa;
		this.ogrSzerokosc = ogrSzerokosc;
		this.ogrPredkosc = ogrPredkosc;
		this.ruchWahadlowy = ruchWahadlowy;
		this.sygnalizacjaSwietlna = sygnalizacjaSwietlna;
		this.awariaMostu = awariaMostu;
		this.ruch2Kierunkowy = ruch2Kierunkowy;
		this.drogaZamknieta = drogaZamknieta;
		this.czasyOczekiwania = czasyOczekiwania;
	}

	/**
	 * Returns the destination of the message, as read from the message data.
	 * 
	 * <p>If any part of the description specifies a destination (“kierunek X”), then that part is used.
	 * Enclosing junctions cannot be used here as we do not know the direction of the location yet.
	 * 
	 * <p>Destinations read from the built-in road attribute list are ignored here, as parsing them
	 * requires a call to {@link #extractDirection()}, which in turn calls this method. For a destination
	 * which can be displayed to the user, first determine the direction, then look up the destination
	 * from the list, and failing that, resort to this method. 
	 */
	public String extractDestination() {
		if (destination != null)
			return destination;
		for (String keyword : KEYWORDS_DESTINATION) {
			int kwStart = this.nazwaOdcinka.indexOf(keyword);
			if (kwStart > -1) {
				destination = this.nazwaOdcinka.substring(kwStart + keyword.length()).trim();
				if (destination.endsWith(")") && !destination.contains("("))
					destination = destination.substring(0, destination.length() - 1);
				break;
			}
		}
		return destination;
	}

	public Direction extractDirection() {
		/* look for keywords in location description */
		String location = this.nazwaOdcinka + " ";
		for (String keyword : KEYWORDS_DIRECTIONALITY_FORWARD)
			if (location.indexOf(keyword) > -1)
				return Direction.FORWARD;
		for (String keyword : KEYWORDS_DIRECTIONALITY_BACKWARD)
			if (location.indexOf(keyword) > -1)
				return Direction.BACKWARD;

		/* examine kilometric points ("od km ... do km ...") in objazd */
		if ((this.objazd != null) && (this.dl > 0)) {
			int kmPos = this.objazd.indexOf(formatKm(this.km));
			if (kmPos > -1) {
				int kmDlPos = this.objazd.indexOf(formatKm(this.km + this.dl));
				if (kmDlPos > -1) {
					if (kmPos < kmDlPos)
						return Direction.FORWARD;
					else if (kmPos > kmDlPos)
						return Direction.BACKWARD;
				}
			}
		}

		/* examine delays for direction information */
		boolean hasForward = false;
		boolean hasBackward = false;
		if ((this.czasyOczekiwania.length == 1)
				&& ("O".equalsIgnoreCase(this.czasyOczekiwania[0].kierunek))
				&& (Duration.ZERO.equals(this.czasyOczekiwania[0].pnPt))
				&& (Duration.ZERO.equals(this.czasyOczekiwania[0].so))
				&& (Duration.ZERO.equals(this.czasyOczekiwania[0].ni))) {
			/* dummy entry, no useful information here */
		} else {
			boolean hasForwardDelay = false;
			boolean hasBackwardDelay = false;
			for (GddkiaDelay delay : this.czasyOczekiwania) {
				if ("O".equalsIgnoreCase(delay.kierunek) || "P".equalsIgnoreCase(delay.kierunek)) {
					hasForward = true;
					if (!(Duration.ZERO.equals(delay.pnPt) && Duration.ZERO.equals(delay.so)
							&& Duration.ZERO.equals(delay.ni)))
						hasForwardDelay = true;
				}
				if ("O".equalsIgnoreCase(delay.kierunek) || "L".equalsIgnoreCase(delay.kierunek)) {
					hasBackward = true;
					if (!(Duration.ZERO.equals(delay.pnPt) && Duration.ZERO.equals(delay.so)
							&& Duration.ZERO.equals(delay.ni)))
						hasBackwardDelay = true;
				}
			}
			if (hasForwardDelay && !hasBackwardDelay)
				return Direction.FORWARD;
			else if (!hasForwardDelay && hasBackwardDelay)
				return Direction.BACKWARD;
		}

		/* examine destination */
		String destination = extractDestination();
		if (destination != null) {
			Distance distance = getDestinationDistance(this.nrDrogi, destination);
			if (distance != null) {
				if (distance.asUnit(Distance.UNIT_KM) >= (km + dl))
					return Direction.FORWARD;
				else if (distance.asUnit(Distance.UNIT_KM) <= km)
					return Direction.BACKWARD;
			}
			// TODO else examine coordinates
		}

		if (hasForward && !hasBackward)
			return Direction.FORWARD;
		else if (!hasForward && hasBackward)
			return Direction.BACKWARD;

		return null;
	}

	/**
	 * Determines the directionality of the message.
	 * 
	 * @return true if unidirectional, false if bidirectional
	 */
	public Boolean extractDirectionality() {
		/* Contraflow always affects both directions */
		if (Boolean.TRUE.equals(this.ruch2Kierunkowy))
			return false;

		/* Single alternate-line traffic is always bidirectional */
		if (Boolean.TRUE.equals(this.ruchWahadlowy))
			return false;
		if ("S06".equalsIgnoreCase(this.skutki) || "Ruch wahadłowy".equalsIgnoreCase(this.skutki))
			return false;

		/* look for keywords in location description */
		String location = this.nazwaOdcinka + " ";
		for (String keyword : KEYWORDS_DIRECTIONALITY)
			if (location.indexOf(keyword) > -1)
				return true;

		if (this.objazd != null) {
			/* look for kilometric points ("od km ... do km ...") in objazd */
			/*
			 * FIXME this fails for the following:
			 * km: 407.490
			 * dl: 2.100
			 * objazd: […] km 407+490 str. P, 407+460, str. P+L, 408+040, str. P+L, 408+965, str. L, 409+590 str. L
			 * Although meant to refer to both directions (affecting different stretches), this gets misread
			 * as a unidirectional location.
			 */
			if (this.dl > 0) {
				int kmPos = this.objazd.indexOf(formatKm(this.km));
				if (kmPos > -1) {
					int kmDlPos = this.objazd.indexOf(formatKm(this.km + this.dl));
					if (kmDlPos > -1)
						return true;
				}
			}
		}

		/* examine delays for direction information */
		boolean hasForward = false;
		boolean hasBackward = false;
		if ((this.czasyOczekiwania.length == 1)
				&& ("O".equalsIgnoreCase(this.czasyOczekiwania[0].kierunek))
				&& (Duration.ZERO.equals(this.czasyOczekiwania[0].pnPt))
				&& (Duration.ZERO.equals(this.czasyOczekiwania[0].so))
				&& (Duration.ZERO.equals(this.czasyOczekiwania[0].ni))) {
			/* dummy entry, no useful information here */
		} else {
			boolean hasForwardDelay = false;
			boolean hasBackwardDelay = false;
			for (GddkiaDelay delay : this.czasyOczekiwania) {
				if ("O".equalsIgnoreCase(delay.kierunek) || "P".equalsIgnoreCase(delay.kierunek)) {
					hasForward = true;
					if (!(Duration.ZERO.equals(delay.pnPt) && Duration.ZERO.equals(delay.so)
							&& Duration.ZERO.equals(delay.ni)))
						hasForwardDelay = true;
				}
				if ("O".equalsIgnoreCase(delay.kierunek) || "L".equalsIgnoreCase(delay.kierunek)) {
					hasBackward = true;
					if (!(Duration.ZERO.equals(delay.pnPt) && Duration.ZERO.equals(delay.so)
							&& Duration.ZERO.equals(delay.ni)))
						hasBackwardDelay = true;
				}
			}
			if (hasForwardDelay && hasBackwardDelay)
				return false;
			else if (hasForwardDelay || hasBackwardDelay)
				return true;
		}

		boolean isMotorwayLike = this.nrDrogi.startsWith("A") || this.nrDrogi.startsWith("S");

		/* Positive for unidirectional, negative for bidirectional */
		int score = (hasForward && hasBackward) ? -1 : (hasForward || hasBackward) ? 1 : 0;

		/* count destination indications in objazd */
		if (this.objazd != null) {
			int destCount = 0;
			for (String keyword : KEYWORDS_DESTINATION_COUNT) {
				String tmp = this.objazd;
				while (tmp.indexOf(keyword) > -1) {
					destCount++;
					tmp = tmp.substring(tmp.indexOf(keyword) + keyword.length());
				}
			}
			if (destCount == 1)
				score++;
			else if (destCount > 1)
				score--;
		}

		/* Examine combinations of events, causes, road type and length */
		if (isMotorwayLike) {
			for (String cause : this.rodzaj)
				if ("I11".equalsIgnoreCase(cause)
						|| "I12".equalsIgnoreCase(cause)
						|| "R14".equalsIgnoreCase(cause))
					score++;
			if ((this.dl) > 0 && ("J05".equalsIgnoreCase(this.skutki)
					|| "Zator drogowy".equalsIgnoreCase(this.skutki)))
				score++;
			if ("S07".equalsIgnoreCase(this.skutki)
					|| "S08".equalsIgnoreCase(this.skutki)
					|| "S09".equalsIgnoreCase(this.skutki)
					|| "Zablokowany pas ruchu".equalsIgnoreCase(this.skutki)
					|| "Zablokowana część pasa ruchu".equalsIgnoreCase(this.skutki)
					|| "Zablokowane pobocze".equalsIgnoreCase(this.skutki))
				score++;
		} else {
			for (String cause : this.rodzaj)
				if ("I02".equalsIgnoreCase(cause)
						|| "I05".equalsIgnoreCase(cause)
						|| "I06".equalsIgnoreCase(cause)
						|| "I07".equalsIgnoreCase(cause)
						|| "I08".equalsIgnoreCase(cause)
						|| "I09".equalsIgnoreCase(cause)
						|| "I10".equalsIgnoreCase(cause)
						|| "I11".equalsIgnoreCase(cause)
						|| "I13".equalsIgnoreCase(cause)
						|| "K00".equalsIgnoreCase(cause)
						|| "K01".equalsIgnoreCase(cause)
						|| "K02".equalsIgnoreCase(cause)
						|| "K04".equalsIgnoreCase(cause)
						|| "R00".equalsIgnoreCase(cause)
						|| "U27".equalsIgnoreCase(cause))
					score--;
		}

		if (score > 0)
			return true;
		else if (score < 0)
			return false;
		else
			return null;
	}

	/**
	 * Returns the junctions enclosing the event, verified and sorted.
	 * 
	 * <p>This goes beyond {@link #extractJunctions()} in that the strings returned are verified to correspond
	 * to known junction names and sorted as encountered when following the road in its forward direction.
	 * 
	 * <p>Sort order is as encountered when following the road in the forward direction, i.e. in the direction
	 * of increasing kilometric points. If a location refers to the opposite direction, junctions would be
	 * encountered in the opposite order (i.e. first {@code junction[1]}, then {@code junction[0]}).
	 */
	/*
	 * TODO handle different spellings:
	 * Szczecin-Kijewo vs. Szczecin Kijewo
	 * Szczecin vs. Szczecin Południe
	 * Szczecin Południe vs. Szczecin południe
	 * Szczecin vs. SZCZECIN
	 */
	/*
	 * TODO use resolvedJunctions
	 */
	public Junction[] extractEnclosingJunctions() {
		if (enclosingJunctions != null)
			return enclosingJunctions;

		/* tickle the cache, no need to store return values locally */
		extractJunctions();
		extractEndpoints();
		if (roadJunctions == null)
			roadJunctions = junctionsByName.getByName(extractRoadRef());

		enclosingJunctions = new Junction[2];
		enclosingJunctions[0] = null;
		enclosingJunctions[1] = null;

		int junctionScore = 0;
		if (roadJunctions == null)
			return enclosingJunctions;
		for (int i = 0; i < 2; i++) {
			if (rawJunctions[i] == null)
				continue;
			if (roadJunctions.containsKey(rawJunctions[i])) {
				enclosingJunctions[i] = JunctionUtils.mergeJunctions(roadJunctions.get(rawJunctions[i]));
				for (Junction junction : roadJunctions.get(rawJunctions[i]))
					if (junction.distance != null) {
						if (junction.distance.asUnit(Distance.UNIT_KM) <= km)
							junctionScore += (i == 0) ? 1 : -1;
						else if (junction.distance.asUnit(Distance.UNIT_KM) >= (km + dl))
							junctionScore += (i == 0) ? -1 : 1;
					}
				if ((endpoints != null) && endpoints.containsKey(TraffLocation.Point.Role.FROM)
						&& endpoints.containsKey(TraffLocation.Point.Role.TO))
					for (Junction junction : roadJunctions.get(rawJunctions[i]))
						if (junction.geo.distanceTo(endpoints.get(TraffLocation.Point.Role.FROM).geo)
								< junction.geo.distanceTo(endpoints.get(TraffLocation.Point.Role.TO).geo))
							junctionScore += (i == 0) ? 1 : -1;
						else if (junction.geo.distanceTo(endpoints.get(TraffLocation.Point.Role.FROM).geo)
								> junction.geo.distanceTo(endpoints.get(TraffLocation.Point.Role.TO).geo))
							junctionScore += (i == 0) ? -1 : 1;
			} else {
				continue;
			}
		}
		if ((enclosingJunctions[0] != null) && (enclosingJunctions[1] != null)
				&& roadJunctions.containsKey(enclosingJunctions[0].name))
			for (Junction junction0 : roadJunctions.get(enclosingJunctions[0].name))
				if ((junction0.distance != null) && roadJunctions.containsKey(enclosingJunctions[1].name))
					for (Junction junction1 : roadJunctions.get(enclosingJunctions[1].name))
						if (junction1.distance != null) {
							if (junction0.distance.asUnit(Distance.UNIT_KM) < junction1.distance.asUnit(Distance.UNIT_KM))
								junctionScore += 2;
							else if (junction0.distance.asUnit(Distance.UNIT_KM) > junction1.distance.asUnit(Distance.UNIT_KM))
								junctionScore -= 2;
						}
		if (junctionScore == 0) {
			enclosingJunctions[0] = null;
			enclosingJunctions[1] = null;
		} else if (junctionScore < 0) {
			Junction tmp = enclosingJunctions[0];
			enclosingJunctions[0] = enclosingJunctions[1];
			enclosingJunctions[1] = tmp;
		}

		return enclosingJunctions;
	}

	/**
	 * Retrieves the junction names of the endpoints from the message, if any.
	 * 
	 * <p>If the location begins or ends close to a junction, its name is returned.
	 * 
	 * <p>Sort order is as encountered when following the road in the forward direction, i.e. in the direction
	 * of increasing kilometric points. If a location refers to the opposite direction, junctions would be
	 * encountered in the opposite order (i.e. first {@code junction[1]}, then {@code junction[0]}).
	 */
	public Junction[] extractEndpointJunctions() {
		if (endpointJunctions != null)
			return endpointJunctions;

		/* update the cache (return if we have no endpoints) */
		extractEndpoints();
		if (endpoints == null)
			return null;
		extractEnclosingJunctions();
		if (roadJunctions == null)
			roadJunctions = junctionsByName.getByName(extractRoadRef());

		endpointJunctions = new Junction[] {null, null};
		TraffLocation.Point.Role [] roles = { TraffLocation.Point.Role.FROM, TraffLocation.Point.Role.TO };
		Set<Junction> candidateJunctions[] = new Set[] {new HashSet<Junction>(), new HashSet<Junction>()};
		boolean examineCandidates = false;

		for (int i = 0; i < 2; i++) {
			if (!endpoints.containsKey(roles[i]))
				continue;
			if (enclosingJunctions[i] != null) {
				if ((enclosingJunctions[i].distance != null)
						&& ((Math.floor(enclosingJunctions[i].distance.asUnit(Distance.UNIT_KM)) == Math.floor(endpoints.get(roles[i]).nominalDistance))
								|| (Math.ceil(enclosingJunctions[i].distance.asUnit(Distance.UNIT_KM)) == Math.ceil(endpoints.get(roles[i]).nominalDistance))))
					endpointJunctions[i] = enclosingJunctions[i];
				else if (enclosingJunctions[i].geo.distanceTo(endpoints.get(roles[i]).geo) < 1.0f)
					endpointJunctions[i] = enclosingJunctions[i];
			} else if (roadJunctions != null) {
				/*
				 * Identify candidate junctions by their coordinates:
				 * If we have multiple junctions with the same name (usually a pair of junctions on opposite
				 * carriageways) and the endpoint is within their bounding box, each of these junction
				 * points is a candidate.
				 * Otherwise, every junction point within 0.5 km of the endpoint is a candidate.
				 */
				for (Set<Junction> junctionSet : roadJunctions.values()) {
					BoundingBox bbox = null;
					for (Junction junction : junctionSet)
						if (bbox == null)
							bbox = new BoundingBox(junction.geo);
						else
							bbox = bbox.extend(junction.geo);
					if (bbox.contains(endpoints.get(roles[i]).geo))
						candidateJunctions[i].addAll(junctionSet);
					else
						for (Junction junction : junctionSet)
							if (junction.geo.distanceTo(endpoints.get(roles[i]).geo) < 0.5f)
								candidateJunctions[i].add(junction);
				}
				if (!candidateJunctions[i].isEmpty())
					examineCandidates = true;
			}
		}
		if (examineCandidates)
			/*
			 * If the sets of candidate junctions overlap, discard all, thus avoiding pairings such as
			 * “between Nowa Wieś and Nowa Wieś”.
			 */
			for (Junction junction : candidateJunctions[0])
				if (candidateJunctions[1].contains(junction)) {
					examineCandidates = false;
					break;
				}
		if (examineCandidates)
			/* If we have an unambiguous junction (i.e. at least the name being consistent), use it */
			for (int i = 0; i < 2; i++)
				if (!candidateJunctions[i].isEmpty()) {
					Junction junction = JunctionUtils.mergeJunctions(candidateJunctions[i]);
					if (junction.name != null)
						endpointJunctions[i] = junction;
				}

		return endpointJunctions;
	}

	/**
	 * Retrieves endpoint coordinates from a message.
	 * 
	 * <p>If no suitable endpoints are found, the result is null. Otherwise, the result is a map of endpoints
	 * and their roles.
	 * 
	 * <p>Roles always refer to the forward direction of the road, i.e. the distance indications for the FROM,
	 * AT and TO points will always be in ascending order. For locations referring to the backward direction
	 * of the road, the FROM and TO roles would need to be swapped.
	 * 
	 * <p>The caller should not make any assumptions on the number of points returned: a zero-length location
	 * may be resolved to the two location markers enclosing it. Conversely, a location with a nonzero length
	 * may be resolved to the only known location marker located in its vicinity.
	 */
	public Map<TraffLocation.Point.Role, Point> extractEndpoints() {
		if (endpoints != null)
			return endpoints;
		endpoints = new HashMap<TraffLocation.Point.Role, Point>();
		/*
		 * Some magic to deal with sections whose kilometric point numbering differs from the rest of the
		 * road: these are designated with an extra letter in nrDrogi (e.g. S5h instead of S5), but OSM does
		 * not consistently supply the section ref. Therefore, we have to fill gaps with milestones for the
		 * road ref, while taking care not to introduce bogus kilometric points causing incorrect results.
		 */
		NavigableMap<Distance, Set<Milestone>> roadMilestones = milestones.get(nrDrogi);
		if (!nrDrogi.equals(extractRoadRef())) {
			if (roadMilestones != null)
				MilestoneUtils.mergeConsistent(roadMilestones,
						MilestoneUtils.toSet(milestones.get(extractRoadRef())), REDUNDANCY_TOLERANCE_KM, Distance.UNIT_KM);
			else
				roadMilestones = milestones.get(extractRoadRef());
		}
		if (roadMilestones == null) {
			LOG.debug("Could not determine location for {} @ {} + {}: no milestones found for road",
					nrDrogi, km, dl);
			return null;
		}

		/* clip to the bounding box of the voivodeship, if known */
		Territory territory = extractTerritory();
		if (territory != null)
			roadMilestones = MilestoneUtils.clipTo(roadMilestones, territory.bbox);

		roadMilestones = MilestoneUtils.clusterWithin(roadMilestones, CLUSTER_DISTANCE_KM, Distance.UNIT_KM);

		Milestone geoMilestone = null;
		if ((geoLat != null) && (geoLong != null))
			geoMilestone = new Milestone(new Distance(km, Distance.UNIT_KM), new LatLon(geoLat, geoLong));
		if (dl > 0) {
			/* Stretch of road */
			Distance fromDist = new Distance(km, Distance.UNIT_KM);
			Distance toDist = new Distance(km + dl, Distance.UNIT_KM);

			/*
			 * Build candidate maps for each point,
			 * eliminate redundant milestones (keeping only the one closest to the nominal distance on each side)
			 * and interpolate where possible
			 */
			NavigableMap<Distance, Set<Milestone>> fromMap =
					MilestoneUtils.eliminateRedundantPerSide(roadMilestones, fromDist, REDUNDANCY_TOLERANCE_KM, Distance.UNIT_KM);
			MilestoneUtils.interpolate(fromMap, fromDist, Math.min(dl / 2.0f, INTERPOLATION_TOLERANCE_KM), Distance.UNIT_KM);
			fromMap = MilestoneUtils.clusterWithin(fromMap, CLUSTER_DISTANCE_KM, Distance.UNIT_KM);
			NavigableMap<Distance, Set<Milestone>> toMap =
					MilestoneUtils.eliminateRedundantPerSide(roadMilestones, toDist, REDUNDANCY_TOLERANCE_KM, Distance.UNIT_KM);
			MilestoneUtils.interpolate(toMap, toDist, Math.min(dl / 2.0f, INTERPOLATION_TOLERANCE_KM), Distance.UNIT_KM);
			toMap = MilestoneUtils.clusterWithin(toMap, CLUSTER_DISTANCE_KM, Distance.UNIT_KM);

			/*
			 * Truncate candidate maps:
			 * no further than the opposite point on one end,
			 * no further than DIST_TOLERANCE_KM on the other
			 */
			Distance minDist = new Distance(Math.max(km - DIST_TOLERANCE_KM, 0), Distance.UNIT_KM);
			Distance maxDist = new Distance(km + dl + DIST_TOLERANCE_KM, Distance.UNIT_KM);
			fromMap = fromMap.subMap(minDist, true, toDist, true);
			toMap = toMap.subMap(fromDist, true, maxDist, true);

			/* If coords are given, eliminate all points which are inconsistent with them */
			if (geoMilestone != null ) {
				Set<Milestone> geoSet = new HashSet<Milestone>();
				geoSet.add(geoMilestone);
				fromMap = MilestoneUtils.eliminateInconsistent(fromMap, geoSet, GEO_CONSISTENCY_TOLERANCE_KM, Distance.UNIT_KM);
				toMap = MilestoneUtils.eliminateInconsistent(toMap, geoSet, GEO_CONSISTENCY_TOLERANCE_KM, Distance.UNIT_KM);
			}

			/* Eliminate remaining redundancies from the sets */
			MilestoneUtils.eliminateRedundantBetweenSides(fromMap, fromDist, REDUNDANCY_TOLERANCE_KM, Distance.UNIT_KM, false);
			MilestoneUtils.eliminateRedundantBetweenSides(toMap, toDist, REDUNDANCY_TOLERANCE_KM, Distance.UNIT_KM, true);

			/* We should now have one milestone on each end (else we have no unambiguous solution) */
			Set<Milestone> fromSet = MilestoneUtils.toSet(fromMap);
			Set<Milestone> toSet = MilestoneUtils.toSet(toMap);

			/* If one set has 0 elements and the other has 1, use the single point if it is inside the range */
			if (fromSet.size() + toSet.size() == 1) {
				/* We have only one point, use that if it is inside the range */
				fromMap = fromMap.subMap(fromDist, true, toDist, true);
				toMap = toMap.subMap(fromDist, true, toDist, true);
				fromSet = MilestoneUtils.toSet(fromMap);
				toSet = MilestoneUtils.toSet(toMap);

				/*
				 * If both maps still have one element between them, set both to have that one element.
				 * Later on, that case will get treated as two coinciding end points.
				 */
				if (fromSet.size() == 1)
					toSet = fromSet;
				else if (toSet.size() == 1)
					fromSet = toSet;
			} else if ((fromSet.size() > 1) || (toSet.size() > 1)) {
				/* try to narrow down the location set using the junctions and internal consistency checks */
				/* start with an internal consistency check */
				fromMap = MilestoneUtils.eliminateInconsistent(fromMap, toSet, DIST_TOLERANCE_KM, Distance.UNIT_KM);
				toMap = MilestoneUtils.eliminateInconsistent(toMap, fromSet, DIST_TOLERANCE_KM, Distance.UNIT_KM);
				fromSet = MilestoneUtils.toSet(fromMap);
				toSet = MilestoneUtils.toSet(toMap);
				if ((fromSet.size() > 1) || (toSet.size() > 1)) {
					/* still no luck, try junctions */
					/*
					 * FIXME This may break if we are near a kilometric point equation and the distance of the
					 * junction is based on a different reference.
					 * Telltale signs (non-exhaustive list):
					 *   - km < 1.0f
					 *   - km << dl
					 *   - distance(geo, junction) > km
					 */
					extractResolvedJunctions();
					Set<Milestone> junctionMilestones = new HashSet<Milestone>();
					for (Set<Junction> junctionSet : resolvedJunctions)
						for (Junction junction : junctionSet)
							if (junction.distance != null)
								junctionMilestones.add(new Milestone(junction.distance, junction.geo));
					if (!junctionMilestones.isEmpty()) {
						// FIXME debug
						/*
						LOG.debug("{}@{}+{}: using junctions with milestone information for disambiguation, {} found",
								nrDrogi, km, dl, fromSet.size(), toSet.size());
						 */
						// FIXME end debug
						fromMap = MilestoneUtils.eliminateInconsistent(fromMap, junctionMilestones, DIST_TOLERANCE_KM, Distance.UNIT_KM);
						toMap = MilestoneUtils.eliminateInconsistent(toMap, junctionMilestones, DIST_TOLERANCE_KM, Distance.UNIT_KM);
						fromSet = MilestoneUtils.toSet(fromMap);
						toSet = MilestoneUtils.toSet(toMap);
					}
					if ((fromSet.size() > 1) || (toSet.size() > 1))
						/* TODO can we make sense of one single junction? */
						if (resolvedJunctions[0].isEmpty() || resolvedJunctions[1].isEmpty())
							LOG.debug("Could not determine junctions for {} @ {} + {} on at least one side",
									nrDrogi, km, dl);
						else {
							/* we have at least one matching junction on each side */
							String[] name = {null, null};
							boolean[] isNameAmbiguous = {false, false};
							for (int i = 0; i < 2; i++)
								for (Junction junction : resolvedJunctions[i]) {
									if (junction.name != null) {
										if (name[i] == null)
											name[i] = junction.name;
										else if (!name[i].matches(junction.name)) {
											isNameAmbiguous[i] = true;
											break;
										}
									} // if junction.name != null
								} // for junction
							if (isNameAmbiguous[0] || isNameAmbiguous[1])
								LOG.debug("Could not determine junctions for {} @ {} + {}: ambiguous junction names",
										nrDrogi, km, dl);
							else {
								/* we have exactly one matching junction name per side */
								Junction[] mergedJunctions = {
										JunctionUtils.mergeJunctions(resolvedJunctions[0]),
										JunctionUtils.mergeJunctions(resolvedJunctions[1])
								};
								for (Map<Distance, Set<Milestone>> map : new Map[]{fromMap, toMap}) {
									/* remove all points which are not within a certain radius of either junction */
									Set<Milestone> toRemove = new HashSet<Milestone>();
									for (Set<Milestone> set : map.values())
										for (Milestone milestone : set)
											if ((milestone.coords.distanceTo(mergedJunctions[0].geo) > JUNCTION_MAX_DISTANCE)
													&& (milestone.coords.distanceTo(mergedJunctions[1].geo) > JUNCTION_MAX_DISTANCE))
												toRemove.add(milestone);
									for (Milestone milestone : toRemove)
										if (map.get(milestone.distance).size() == 1)
											map.remove(milestone.distance);
										else
											map.get(milestone.distance).remove(milestone);
								} // for map
								fromSet = MilestoneUtils.toSet(fromMap);
								toSet = MilestoneUtils.toSet(toMap);
								// TODO eliminateInconsistent (only if location is still ambiguous)
							} // if isNameAmbiguous
						}
				}
			} else if ((fromSet.size() == 0) && (toSet.size() == 0)) {
				/* try to use the junctions */
				extractEndpointsUsingJunctions();
			}

			if ((fromSet.size() != 1) || (toSet.size() != 1))
				LOG.debug("Could not determine unambiguous location for {} @ {} + {}: {} and {} milestones found",
						nrDrogi, km, dl, fromSet.size(), toSet.size());
			else if (MilestoneUtils.eliminateInconsistent(fromSet, toSet, REDUNDANCY_TOLERANCE_KM, Distance.UNIT_KM).isEmpty())
				LOG.debug("Could not determine unambiguous location for {} @ {} + {}: milestone pair is not consistent",
						nrDrogi, km, dl);
			else {
				/* exactly one candidate for each end, and it is consistent with the other */
				Milestone fromMilestone = fromSet.iterator().next();
				Milestone toMilestone = toSet.iterator().next();
				if (fromMilestone.distance.equals(toMilestone.distance)) {
					/* special case: both end points coincide */
					Set<Milestone> atSet = fromSet;
					Milestone atMilestone = fromMilestone;
					endpoints.put(TraffLocation.Point.Role.AT, new Point(km, atMilestone.coords, atMilestone.distance.asUnit(Distance.UNIT_KM)));
					NavigableMap<Distance, Set<Milestone>> enclosingMap =
							MilestoneUtils.eliminateInconsistent(roadMilestones, atSet, REDUNDANCY_TOLERANCE_KM, Distance.UNIT_KM);
					Distance key = enclosingMap.lowerKey(atMilestone.distance);
					if ((key != null) && (enclosingMap.get(key).size() == 1))
						endpoints.put(TraffLocation.Point.Role.FROM, new Point(key.asUnit(Distance.UNIT_KM),
								enclosingMap.get(key).iterator().next().coords, key.asUnit(Distance.UNIT_KM)));
					key = enclosingMap.higherKey(atMilestone.distance);
					if ((key != null) && (enclosingMap.get(key).size() == 1))
						endpoints.put(TraffLocation.Point.Role.TO, new Point(key.asUnit(Distance.UNIT_KM),
								enclosingMap.get(key).iterator().next().coords, key.asUnit(Distance.UNIT_KM)));
				} else {
					endpoints.put(TraffLocation.Point.Role.FROM, new Point(km, fromMilestone.coords, fromMilestone.distance.asUnit(Distance.UNIT_KM)));
					endpoints.put(TraffLocation.Point.Role.TO, new Point(km + dl, toMilestone.coords, toMilestone.distance.asUnit(Distance.UNIT_KM)));
				}
			}
		} else {
			/* Single point */
			Distance atDist = new Distance(km, Distance.UNIT_KM);

			/*
			 * Build candidate map,
			 * eliminate redundant milestones (keeping only the one closest to the nominal distance)
			 * and interpolate where possible
			 */
			/*
			 * FIXME Interpolation may produce bogus results when a pair of milestones belonging to different
			 * sequences happen to be consistent. Such conditions are likely when km is very small.
			 */
			NavigableMap<Distance, Set<Milestone>> atMap =
					MilestoneUtils.eliminateRedundantPerSide(roadMilestones, atDist, REDUNDANCY_TOLERANCE_KM, Distance.UNIT_KM);
			MilestoneUtils.interpolate(atMap, atDist, INTERPOLATION_TOLERANCE_KM, Distance.UNIT_KM);

			/* Truncate candidate map: no further than DIST_TOLERANCE_KM on each end */
			Distance atMinDist = new Distance(Math.max(km - DIST_TOLERANCE_KM, 0), Distance.UNIT_KM);
			Distance atMaxDist = new Distance(km + DIST_TOLERANCE_KM, Distance.UNIT_KM);
			atMap = atMap.subMap(atMinDist, true, atMaxDist, true);

			/* If coords are given, eliminate all points which are inconsistent with them */
			if (geoMilestone != null) {
				Set<Milestone> geoSet = new HashSet<Milestone>();
				geoSet.add(geoMilestone);
				atMap = MilestoneUtils.eliminateInconsistent(atMap, geoSet, GEO_CONSISTENCY_TOLERANCE_KM, Distance.UNIT_KM);
			}

			/* Eliminate remaining redundancies from the sets */
			// FIXME this will resolve ties in favor of the lower side, it would be better to turn such pairs into from/to
			MilestoneUtils.eliminateRedundantBetweenSides(atMap, atDist, REDUNDANCY_TOLERANCE_KM, Distance.UNIT_KM, true);

			/* We should now have one milestone (else we have no unambiguous solution) */
			Set<Milestone> atSet = MilestoneUtils.toSet(atMap);
			if (atSet.isEmpty())
				LOG.debug("No matching milestone found for {} @ {}", nrDrogi, km);
			else if (atSet.size() > 1)
				LOG.debug("Ambiguous location {} @ {} ({} milestones found)", nrDrogi, km, atSet.size());
			else {
				/* exactly one candidate left, calculate enclosing (from/to) and add the points to endpoints */
				Milestone atMilestone = atSet.iterator().next();
				endpoints.put(TraffLocation.Point.Role.AT, new Point(km, atMilestone.coords, atMilestone.distance.asUnit(Distance.UNIT_KM)));
				NavigableMap<Distance, Set<Milestone>> enclosingMap =
						MilestoneUtils.eliminateInconsistent(roadMilestones, atSet, REDUNDANCY_TOLERANCE_KM, Distance.UNIT_KM);
				Distance key = enclosingMap.lowerKey(atMilestone.distance);
				if ((key != null) && (enclosingMap.get(key).size() == 1))
					endpoints.put(TraffLocation.Point.Role.FROM, new Point(key.asUnit(Distance.UNIT_KM),
							enclosingMap.get(key).iterator().next().coords, key.asUnit(Distance.UNIT_KM)));
				key = enclosingMap.higherKey(atMilestone.distance);
				if ((key != null) && (enclosingMap.get(key).size() == 1))
					endpoints.put(TraffLocation.Point.Role.TO, new Point(key.asUnit(Distance.UNIT_KM),
							enclosingMap.get(key).iterator().next().coords, key.asUnit(Distance.UNIT_KM)));
			}
		}
		return endpoints;
	}

	/**
	 * Retrieves TraFF events from the message.
	 * 
	 * <p>Events returned will have supplementary information, quantifiers and speed set as appropriate for the
	 * message. Length is currently not supported.
	 */
	public List<TraffEvent> extractEvents() {
		Map<TraffEvent.Type, TraffEvent.Builder> builders = new HashMap<TraffEvent.Type, TraffEvent.Builder>();
		Map<TraffSupplementaryInfo.Type, TraffSupplementaryInfo> si	=
				new HashMap<TraffSupplementaryInfo.Type, TraffSupplementaryInfo>();
		EventMapping mapping;
		TraffEvent.Builder builder;

		/* add events from rodzaj/poz */
		for (String cause : rodzaj) {
			mapping = events.get(cause);
			if (mapping != null) {
				if (mapping.eventType != null) {
					builder = new TraffEvent.Builder();
					builders.put(mapping.eventType, builder);
					builder.setType(mapping.eventType);
					if (mapping.eventClass != null)
						builder.setEventClass(mapping.eventClass);
				}
				if (mapping.siType != null)
					si.put(mapping.siType, new TraffSupplementaryInfo(mapping.siClass, mapping.siType));
			}
		}

		/* add events from skutki */
		mapping = events.get(skutki);
		if (mapping != null) {
			if (mapping.eventType != null) {
				builder = new TraffEvent.Builder();
				builders.put(mapping.eventType, builder);
				builder.setType(mapping.eventType);
				if (mapping.eventClass != null)
					builder.setEventClass(mapping.eventClass);
			}
			if (mapping.siType != null)
				si.put(mapping.siType, new TraffSupplementaryInfo(mapping.siClass, mapping.siType));
		}

		/* add events from typ only if nothing else was found */
		if (builders.isEmpty() && !(typ == null)) {
			if (typ.equalsIgnoreCase("U"))
				mapping = new EventMapping("U", TraffEvent.Type.CONSTRUCTION_ROADWORKS.name(), null);
			else if (typ.equalsIgnoreCase("W"))
				mapping = new EventMapping("W", TraffEvent.Type.INCIDENT_ACCIDENT.name(), null);
			else if (typ.equalsIgnoreCase("I"))
				mapping = new EventMapping("I", TraffEvent.Type.ACTIVITY_EVENT.name(), null);
			else
				mapping = null;

			if ((mapping != null) && (mapping.eventType != null)) {
				builder = new TraffEvent.Builder();
				builders.put(mapping.eventType, builder);
				builder.setType(mapping.eventType);
				if (mapping.eventClass != null)
					builder.setEventClass(mapping.eventClass);
			}
		}

		/* add events from other elements */
		if (ogrNosnosc != null) {
			builder = new TraffEvent.Builder();
			builder.setEventClass(TraffEvent.Class.RESTRICTION);
			builder.setType(TraffEvent.Type.RESTRICTION_MAX_WEIGHT);
			builder.setQuantifier(new WeightQuantifier(ogrNosnosc));
			builders.put(TraffEvent.Type.RESTRICTION_MAX_WEIGHT, builder);
		}
		if (ogrNacisk != null) {
			builder = new TraffEvent.Builder();
			builder.setEventClass(TraffEvent.Class.RESTRICTION);
			builder.setType(TraffEvent.Type.RESTRICTION_MAX_AXLE_LOAD);
			builder.setQuantifier(new WeightQuantifier(ogrNacisk));
			builders.put(TraffEvent.Type.RESTRICTION_MAX_AXLE_LOAD, builder);
		}
		if ((ogrSkrajniaPozioma != null) || (ogrSzerokosc != null)) {
			builder = new TraffEvent.Builder();
			builder.setEventClass(TraffEvent.Class.RESTRICTION);
			builder.setType(TraffEvent.Type.RESTRICTION_MAX_WIDTH);
			Float limit;
			if (ogrSkrajniaPozioma == null)
				limit = ogrSzerokosc;
			else if (ogrSzerokosc == null)
				limit = ogrSkrajniaPozioma;
			else
				limit = Math.min(ogrSkrajniaPozioma, ogrSzerokosc);
			builder.setQuantifier(new DimensionQuantifier(limit));
			builders.put(TraffEvent.Type.RESTRICTION_MAX_WIDTH, builder);
		}
		if (ogrSkrajniaPionowa != null) {
			builder = new TraffEvent.Builder();
			builder.setEventClass(TraffEvent.Class.RESTRICTION);
			builder.setType(TraffEvent.Type.RESTRICTION_MAX_HEIGHT);
			builder.setQuantifier(new DimensionQuantifier(ogrSkrajniaPionowa));
			builders.put(TraffEvent.Type.RESTRICTION_MAX_HEIGHT, builder);
		}
		if (ogrPredkosc != null) {
			builder = new TraffEvent.Builder();
			builder.setEventClass(TraffEvent.Class.RESTRICTION);
			builder.setType(TraffEvent.Type.RESTRICTION_SPEED_LIMIT);
			builder.setSpeed(ogrPredkosc);
			builders.put(TraffEvent.Type.RESTRICTION_SPEED_LIMIT, builder);
		}
		if (Boolean.TRUE.equals(ruchWahadlowy)) {
			builder = new TraffEvent.Builder();
			builder.setEventClass(TraffEvent.Class.RESTRICTION);
			builder.setType(TraffEvent.Type.RESTRICTION_SINGLE_ALTERNATE_LINE_TRAFFIC);
			builders.put(TraffEvent.Type.RESTRICTION_SINGLE_ALTERNATE_LINE_TRAFFIC, builder);
		}
		if (Boolean.TRUE.equals(sygnalizacjaSwietlna)) {
			builder = new TraffEvent.Builder();
			builder.setEventClass(TraffEvent.Class.CONSTRUCTION);
			builder.setType(TraffEvent.Type.CONSTRUCTION_TEMP_TRAFFIC_LIGHT);
			builders.put(TraffEvent.Type.CONSTRUCTION_TEMP_TRAFFIC_LIGHT, builder);
		}
		if (Boolean.TRUE.equals(awariaMostu)) {
			builder = new TraffEvent.Builder();
			builder.setEventClass(TraffEvent.Class.HAZARD);
			builder.setType(TraffEvent.Type.HAZARD_BRIDGE_DAMAGE);
			builders.put(TraffEvent.Type.HAZARD_BRIDGE_DAMAGE, builder);
		}
		if (Boolean.TRUE.equals(ruch2Kierunkowy)) {
			builder = new TraffEvent.Builder();
			builder.setEventClass(TraffEvent.Class.RESTRICTION);
			builder.setType(TraffEvent.Type.RESTRICTION_CONTRAFLOW);
			builders.put(TraffEvent.Type.RESTRICTION_CONTRAFLOW, builder);
		}
		if (Boolean.TRUE.equals(drogaZamknieta)) {
			builder = new TraffEvent.Builder();
			builder.setEventClass(TraffEvent.Class.RESTRICTION);
			builder.setType(TraffEvent.Type.RESTRICTION_CLOSED);
			builders.put(TraffEvent.Type.RESTRICTION_CLOSED, builder);
		}
		// TODO czasyOczekiwania

		/* 
		 * fallback event if no others have been found
		 * (can happen if we don’t support the typ or any of the events and no restrictions are defined)
		 */
		if (builders.isEmpty()) {
			builder = new TraffEvent.Builder();
			builder.setEventClass(TraffEvent.Class.CONGESTION);
			builder.setType(TraffEvent.Type.CONGESTION_TRAFFIC_PROBLEM);
			builders.put(TraffEvent.Type.CONGESTION_TRAFFIC_PROBLEM, builder);
		}

		/* Attach supplementary information to the first item */
		builders.values().iterator().next().addSupplementaryInfos(si.values());

		/* build the array and return it */
		ArrayList<TraffEvent> res = new ArrayList<TraffEvent>();
		for (TraffEvent.Builder b : builders.values())
			res.add(b.build());
		return res;
	}

	/**
	 * Extracts the names of the junctions enclosing the location.
	 * 
	 * <p>This is a highly error-prone operation, as it relies on parsing prose text. Some junctions may fail to
	 * get recognized; in other cases one member will hold both junction names while the other will be null.
	 * 
	 * <p>Junctions are not verified against the map.
	 * 
	 * @return A two-member string array. Junctions are ordered as they appear in the message.
	 */
	// TODO: m. Kostów na trasie Kępno - Byczyna (m. <at> na trasie <from> - <to>)
	// TODO: m. Wołczyn na trasie Namysłów kier. Kluczbork
	// TODO: m. Olesno ul. Kluczborska (m. <...> ul. <at>)
	// TODO: ignore case when comparing
	// TODO: if we get a single junction named A-B (max. 1 hyphen), compare against table
	public String[] extractJunctions() {
		if (rawJunctions != null)
			return rawJunctions;
		rawJunctions = new String[2];
		rawJunctions[0] = null;
		rawJunctions[1] = null;

		/* add trailing spaces to match keywords at end */
		String location = this.nazwaOdcinka + " ";
		int kwStart;

		/* truncate indications following the junction names */
		for (String keyword : KEYWORDS_DESTINATION) {
			kwStart = location.indexOf(keyword);
			if (kwStart > -1)
				location = location.substring(0, kwStart) + " ";
		}
		for (String keyword : KEYWORDS_AFTER_JUNCTION) {
			kwStart = location.indexOf(keyword);
			if (kwStart > -1)
				location = location.substring(0, kwStart) + " ";
		}
		location = location.trim();
		if (location.endsWith(".") || location.endsWith(",") || location.endsWith("-"))
			location = location.substring(0, location.length() - 1).trim();

		String[] tmp = JUNCTION_SEPARATOR_PATTERN.split(location);
		for (int i = 0; i < tmp.length; i++) {
			if (i > 1) {
				rawJunctions[0] = null;
				rawJunctions[1] = null;
				return rawJunctions;
			}
			/* leading space to ensure strings match */
			tmp[i] = " " + tmp[i];
			for (String keyword : KEYWORDS_JUNCTION) {
				kwStart = tmp[i].indexOf(keyword);
				if (kwStart > -1) {
					tmp[i] = tmp[i].substring(kwStart + keyword.length());
					break;
				}
			}
			rawJunctions[i] = tmp[i].trim();
		}
		return rawJunctions;
	}

	public Territory extractTerritory() {
		return Territory.forName(woj);
	}

	/**
	 * Converts the message to a TraFF message.
	 * 
	 * @param updateTime The update time for the TraFF message. This should be the timestamp of the feed in
	 * which the message was received.
	 */
	public TraffMessage toTraff(String sourcePrefix, Date updateTime) {
		return toTraff(sourcePrefix, updateTime, null);
	}

	/**
	 * Extracts the road reference number from a message.
	 * 
	 * <p>Sometimes road numbers like S1f are used for disambiguation of kilometric points: a letter is appended
	 * to denote the section of the road. This method truncates any such suffixes and returns the road number
	 * as used in maps.
	 */
	public String extractRoadRef() {
		String res = nrDrogi;
		if (res.matches("(A|S)?[0-9]+[a-z]"))
			res = res.substring(0, res.length() - 1);
		return res;
	}

	/**
	 * Converts the message to a TraFF message.
	 * 
	 * <p>This method allows previously received messages to be specified as an optional argument. If the ID of
	 * the new message matches that of a message in the collection, the {@code receiveTime} of the old
	 * message is used. 
	 * 
	 * @param updateTime The update time for the TraFF message. This should be the timestamp of the feed in
	 * which the message was received.
	 * @param oldMessages Previously received messages
	 */
	public TraffMessage toTraff(String sourcePrefix, Date updateTime, Collection<TraffMessage> oldMessages) {
		TraffMessage.Builder builder = new TraffMessage.Builder();
		Boolean directionality = extractDirectionality();
		Direction direction = extractDirection();
		/* if the message is unidirectional but the direction is not known, treat directionality as unknown */
		if ((direction == null) && (Boolean.TRUE.equals(directionality)))
			directionality = null;

		String id = String.format(sourcePrefix + ":%s@%.3f,%.3f,%s", nrDrogi, km, dl,
				Boolean.TRUE.equals(directionality) ? (Direction.BACKWARD.equals(direction) ? "L" : "P") : "O");
		builder.setId(id);

		/*
		 * Timestamps:
		 * Receive time is the earliest recorded timestamp (including receive times of existing messages with
		 * the same ID)
		 * Update time is that of the feed
		 * Expiration/end time is indicated in the message (one timestamp which may be either)
		 * Start time is as indicated in the message, if later than receive time (else it is null)
		 */
		builder.setUpdateTime(updateTime);

		Date receiveTime = earlier(dataPowstania, updateTime);
		// TODO dataLikwidacji may be expiration or end time, figure out which
		Date endTime = null;
		Date expirationTime = dataLikwidacji;

		/* enforce consistency with previous messages:
		 * receive time is the earliest timestamp observed
		 * endTime is set when dataLikwidacji has been shortened (may also be in the old message);
		 * if set, it is always equal to dataLikwidacji
		 * expirationTime is the latest timestamp observed
		 */
		if (oldMessages != null)
			for (TraffMessage message : oldMessages)
				if (id.equals(message.id)) {
					receiveTime = earlier(receiveTime, message.receiveTime);
					if (message.endTime != null) {
						endTime = dataLikwidacji;
						expirationTime = later(expirationTime, endTime);
					}
					if ((message.expirationTime != null) && (message.expirationTime.after(dataLikwidacji))) {
						endTime = dataLikwidacji;
						expirationTime = later(expirationTime, later(message.endTime, message.expirationTime));
					}
				}
		builder.setReceiveTime(receiveTime);
		if ((dataPowstania != null) && (dataPowstania.after(receiveTime)))
			builder.setStartTime(dataPowstania);
		builder.setEndTime(endTime);
		builder.setExpirationTime(expirationTime);

		/* Events */
		builder.addEvents(extractEvents());

		/* Assemble the location */
		TraffLocation.Builder locBuilder = new TraffLocation.Builder();
		if (Boolean.TRUE.equals(directionality)) {
			locBuilder.setDirectionality(Directionality.ONE_DIRECTION);
		} else
			/* if directionality is unknown, assume bidirectional */
			locBuilder.setDirectionality(Directionality.BOTH_DIRECTIONS);

		locBuilder.setCountry("PL");
		/* since we are dealing with the national road network, woj is not needed for disambiguation, but we’re including it anyway */
		Territory territory = extractTerritory();
		if (territory != null)
			locBuilder.setTerritory(territory.ref);

		extractEnclosingJunctions();
		Junction[] endpointJunctions = extractEndpointJunctions();

		/*
		 * We can’t get a true origin (and might not be able to get a true destination), therefore we use the
		 * enclosing junctions unless they coincide with the start/end junction.
		 * 
		 * For the moment, origin and destination are relative to the forward direction of the road. If the
		 * location refers to the opposite direction, we will swap them later.
		 * 
		 * If we have a true destination, we will add that later, after swapping the end points (if needed).
		 */
		if (((endpointJunctions == null) || (endpointJunctions[0] == null))
				&& (enclosingJunctions[0] != null))
			locBuilder.setOrigin(enclosingJunctions[0].name);
		if (((endpointJunctions == null) || (endpointJunctions[1] == null))
				&& (enclosingJunctions[1] != null))
			locBuilder.setDestination(enclosingJunctions[1].name);

		locBuilder.setFuzziness(Fuzziness.MEDIUM_RES);
		if ((nrDrogi != null) && !nrDrogi.isEmpty()) {
			locBuilder.setRoadRef(extractRoadRef());
			if (nrDrogi.startsWith("A"))
				locBuilder.setRoadClass(RoadClass.MOTORWAY);
			else if (nrDrogi.startsWith("S"))
				locBuilder.setRoadClass(RoadClass.TRUNK);
			else
				locBuilder.setRoadClass(RoadClass.PRIMARY);
		}
		Map<TraffLocation.Point.Role, Point> endpoints = extractEndpoints();
		if ((endpoints == null) || (endpoints.isEmpty()))
			// TODO error message?
			return null;
		for (Map.Entry<TraffLocation.Point.Role, Point> endpoint : endpoints.entrySet()) {
			TraffLocation.Point.Builder pBuilder = new TraffLocation.Point.Builder();
			pBuilder.setCoordinates(endpoint.getValue().geo);
			pBuilder.setDistance(endpoint.getValue().nominalDistance);
			if ((endpoint.getKey() == TraffLocation.Point.Role.FROM)
					&& !endpoints.containsKey(TraffLocation.Point.Role.AT)) {
				pBuilder.setDistance(km);
				if (endpointJunctions[0] != null) {
					pBuilder.setJunctionName(endpointJunctions[0].name);
					pBuilder.setJunctionRef(endpointJunctions[0].ref);
				}
			} else if ((endpoint.getKey() == TraffLocation.Point.Role.TO)
				&& !endpoints.containsKey(TraffLocation.Point.Role.AT)) {
				pBuilder.setDistance(km + dl);
				if (endpointJunctions[1] != null) {
					pBuilder.setJunctionName(endpointJunctions[1].name);
					pBuilder.setJunctionRef(endpointJunctions[1].ref);
				}
			} else if ((endpoint.getKey() == TraffLocation.Point.Role.AT) && (dl == 0))
				pBuilder.setDistance(km);
			locBuilder.setPoint(endpoint.getKey(), pBuilder.build());
		}
		if (Direction.BACKWARD.equals(direction))
			locBuilder.invert();
		/* add attributes from resource (for destination, fall back to data extracted from the message) */
		Map<String, String> attrs = roadAttributes.get(nrDrogi);
		String destination = null;
		if (attrs != null)
			destination = attrs.get(Direction.BACKWARD.equals(direction) ? ATTR_ORIGIN : ATTR_DESTINATION);
		if (destination == null)
			destination = extractDestination();
		if (destination != null)
			locBuilder.setDestination(destination);

		if (attrs != null) {
			String origin = attrs.get(Direction.BACKWARD.equals(direction) ? ATTR_DESTINATION : ATTR_ORIGIN);
			if (origin != null)
				locBuilder.setOrigin(origin);
			String roadIsUrban = attrs.get(ATTR_ROAD_IS_URBAN);
			if (roadIsUrban != null)
				locBuilder.setRoadIsUrban(Boolean.valueOf(roadIsUrban));
			String roadName = attrs.get(ATTR_ROAD_NAME);
			if (roadName != null)
				locBuilder.setRoadName(roadName);
			String town = attrs.get(ATTR_TOWN);
			if (town != null)
				locBuilder.setTown(town);
		} else if (nrDrogi.matches("(A|S)?[0-9]+[a-z]"))
			/* if the road number has a suffix and we have no other attributes, use that as the road name */
			locBuilder.setRoadName(nrDrogi);

		builder.setLocation(locBuilder.build());

		return builder.build();
	}

	public static class Builder {
		String typ = null;
		String nrDrogi = null;
		String woj = null;
		Float km = null;
		Float dl = null;
		Float geoLat = null;
		Float geoLong = null;
		String nazwaOdcinka = null;
		Date dataPowstania = null;
		Date dataLikwidacji = null;
		String objazd = null;
		// objazdMapy is not represented
		HashSet<String> rodzaj = new HashSet<String>();
		String skutki = null;
		Float ogrNosnosc = null;
		Float ogrNacisk = null;
		Float ogrSkrajniaPozioma = null;
		Float ogrSkrajniaPionowa = null;
		Float ogrSzerokosc = null;
		Integer ogrPredkosc = null;
		Boolean ruchWahadlowy = null;
		Boolean sygnalizacjaSwietlna = null;
		Boolean awariaMostu = null;
		Boolean ruch2Kierunkowy = null;
		Boolean drogaZamknieta = null;
		ArrayList<GddkiaDelay> czasyOczekiwania = new ArrayList<GddkiaDelay>();

		public void addRodzaj(String poz) {
			rodzaj.add(poz);
		}

		public void addRodzaj(Collection<String> rodzaj) {
			rodzaj.addAll(rodzaj);
		}

		public void addCzasOczekiwania(GddkiaDelay czasOczekiwania) {
			czasyOczekiwania.add(czasOczekiwania);
		}

		public void addCzasyOczekiwania(Collection<GddkiaDelay> czasyOczekiwania) {
			czasyOczekiwania.addAll(czasyOczekiwania);
		}

		public GddkiaMessage build() {
			return new GddkiaMessage(typ, nrDrogi, woj, km, dl, geoLat, geoLong, nazwaOdcinka, dataPowstania,
					dataLikwidacji, objazd, rodzaj.toArray(new String[0]), skutki, ogrNosnosc, ogrNacisk,
					ogrSkrajniaPozioma, ogrSkrajniaPionowa, ogrSzerokosc, ogrPredkosc, ruchWahadlowy,
					sygnalizacjaSwietlna, awariaMostu, ruch2Kierunkowy, drogaZamknieta,
					czasyOczekiwania.toArray(new GddkiaDelay[0]));
		}

		public void clearRodzaj() {
			rodzaj.clear();
		}

		public void clearCzasyOczekiwania() {
			czasyOczekiwania.clear();
		}

		public void setTyp(String typ) {
			this.typ = typ;
		}

		public void setNrDrogi(String nrDrogi) {
			this.nrDrogi = (nrDrogi == null) ? null : nrDrogi.trim();
		}

		public void setWoj(String woj) {
			this.woj = woj;
		}

		public void setKm(Float km) {
			this.km = km;
		}

		public void setDl(Float dl) {
			this.dl = dl;
		}

		public void setGeoLat(Float geoLat) {
			this.geoLat = geoLat;
		}

		public void setGeoLong(Float geoLong) {
			this.geoLong = geoLong;
		}

		public void setNazwaOdcinka(String nazwaOdcinka) {
			this.nazwaOdcinka = nazwaOdcinka;
		}

		public void setDataPowstania(Date dataPowstania) {
			this.dataPowstania = dataPowstania;
		}

		public void setDataLikwidacji(Date dataLikwidacji) {
			this.dataLikwidacji = dataLikwidacji;
		}

		public void setObjazd(String objazd) {
			this.objazd = objazd;
		}

		public void setSkutki(String skutki) {
			this.skutki = skutki;
		}

		public void setOgrNosnosc(Float ogrNosnosc) {
			this.ogrNosnosc = ogrNosnosc;
		}

		public void setOgrNacisk(Float ogrNacisk) {
			this.ogrNacisk = ogrNacisk;
		}

		public void setOgrSkrajniaPozioma(Float ogrSkrajniaPozioma) {
			this.ogrSkrajniaPozioma = ogrSkrajniaPozioma;
		}

		public void setOgrSkrajniaPionowa(Float ogrSkrajniaPionowa) {
			this.ogrSkrajniaPionowa = ogrSkrajniaPionowa;
		}

		public void setOgrSzerokosc(Float ogrSzerokosc) {
			this.ogrSzerokosc = ogrSzerokosc;
		}

		public void setOgrPredkosc(Integer ogrPredkosc) {
			this.ogrPredkosc = ogrPredkosc;
		}

		public void setRuchWahadlowy(Boolean ruchWahadlowy) {
			this.ruchWahadlowy = ruchWahadlowy;
		}

		public void setSygnalizacjaSwietlna(Boolean sygnalizacjaSwietlna) {
			this.sygnalizacjaSwietlna = sygnalizacjaSwietlna;
		}

		public void setAwariaMostu(Boolean awariaMostu) {
			this.awariaMostu = awariaMostu;
		}

		public void setRuch2Kierunkowy(Boolean ruch2Kierunkowy) {
			this.ruch2Kierunkowy = ruch2Kierunkowy;
		}

		public void setDrogaZamknieta(Boolean drogaZamknieta) {
			this.drogaZamknieta = drogaZamknieta;
		}
	}

	/**
	 * Represents one of the points which defines a traffic location.
	 */
	public static class Point {
		/**
		 * The coordinates of the point.
		 */
		public final LatLon geo;

		/**
		 * The kilometric point to which this point refers (in km).
		 * 
		 * <p>The nominal kilometric point is taken directly from the feed, without any further adaptations. It
		 * may differ from the location indicated by {@link #geo}.
		 */
		public final Float nominalDistance;

		/**
		 * The kilometric point to which {@link #geo} refers (in km).
		 * 
		 * <p>This may differ slightly from {@link #nominalDistance}, the nominal distance indicated in the
		 * feed. Typically, it is rounded to the nearest multiple of 1 km; more generically, it represents
		 * the kilometric point closest to {@link #nominalDistance}. 
		 */
		public final float actualDistance;

		private Point(Float nominalDistance, LatLon geo, float actualDistance) {
			super();
			this.nominalDistance = nominalDistance;
			this.geo = geo;
			this.actualDistance = actualDistance;
		}
	}

	public static class Territory {
		private static Map<String, Territory> territories;

		static {
			territories = new HashMap<String, Territory>();
			territories.put("dolnośląskie", new Territory("DS", new BoundingBox(50.0963f, 14.8174f, 51.8048f, 17.7953f)));
			territories.put("kujawsko-pomorskie", new Territory("KP", new BoundingBox(52.3306f, 17.2473f, 53.7811f, 19.7616f)));
			territories.put("lubelskie", new Territory("LU", new BoundingBox(50.2518f, 21.6155f, 52.2878f, 24.1458f)));
			territories.put("lubuskie", new Territory("LB", new BoundingBox(51.3632f, 14.5341f, 53.1239f, 16.4168f)));
			territories.put("łódzkie", new Territory("LD", new BoundingBox(50.8433f, 18.0746f, 52.3941f, 20.6593f)));
			territories.put("małopolskie", new Territory("MA", new BoundingBox(49.1785f, 19.0831f, 50.5205f, 21.4217f)));
			territories.put("mazowieckie", new Territory("MZ", new BoundingBox(51.0131f, 19.2592f, 53.4818f, 23.1284f)));
			territories.put("opolskie", new Territory("OP", new BoundingBox(49.9725f, 16.9079f, 51.1946f, 18.6955f)));
			territories.put("podkarpackie", new Territory("PK", new BoundingBox(49.002f, 21.14f, 50.8205f, 23.5479f)));
			territories.put("podlaskie", new Territory("PD", new BoundingBox(52.2799f, 21.5928f, 54.4104f, 23.9463f)));
			territories.put("pomorskie", new Territory("PM", new BoundingBox(53.491f, 16.6991f, 54.8358f, 19.6486f)));
			territories.put("śląskie", new Territory("SL", new BoundingBox(49.394f, 18.035f, 51.0994f, 19.9741f)));
			territories.put("świętokrzyskie", new Territory("SK", new BoundingBox(50.1855f, 19.7043f, 51.3425f, 21.8693f)));
			territories.put("warmińsko-mazurskie", new Territory("WN", new BoundingBox(53.1391f, 19.1276f, 54.4532f, 22.8058f)));
			territories.put("wielkopolskie", new Territory("WP", new BoundingBox(51.1038f, 15.7763f, 53.6559f, 19.105f)));
			territories.put("zachodniopomorskie", new Territory("ZP", new BoundingBox(52.6243f, 14.123f, 54.57f, 16.9821f)));
		}

		public final String ref;
		public final BoundingBox bbox;

		private Territory(String ref, BoundingBox bbox) {
			super();
			this.ref = ref;
			this.bbox = bbox;
		}

		public static Territory forName(String name) {
			return territories.get(name);
		}
	}

	static class EventMapping {
		final String gddkiaEvent;
		final TraffEvent.Class eventClass;
		final TraffEvent.Type eventType;
		final TraffSupplementaryInfo.Class siClass;
		final TraffSupplementaryInfo.Type siType;

		private EventMapping(String gddkiaEvent, String eventType, String siType) {
			super();
			this.gddkiaEvent = gddkiaEvent;

			/* infer eventClass from eventType */
			if (eventType == null)
				this.eventClass = null;
			else {
				String eventClass = eventType;
				TraffEvent.Class tmpEventClass = null;
				while ((tmpEventClass == null) && !eventClass.isEmpty()) {
					try {
						tmpEventClass = TraffEvent.Class.valueOf(eventClass);
					} catch (IllegalArgumentException|NullPointerException e) {
						if (eventClass.contains("_"))
							eventClass = eventClass.substring(0, eventClass.lastIndexOf('_'));
						else
							eventClass = "";
					}
				}
				this.eventClass = tmpEventClass;
			}

			TraffEvent.Type tmpEventType;
			try {
				tmpEventType = TraffEvent.Type.valueOf(eventType);
			} catch (IllegalArgumentException|NullPointerException e) {
				if ((eventType != null) && !eventType.isEmpty())
					LOG.warn("Cannot create mapping for unknown TraFF event {}", eventType);
				tmpEventType = null;
			}
			this.eventType = tmpEventType;

			/* infer siClass from siType */
			if ((siType == null) || !siType.startsWith("S_"))
				this.siClass = null;
			else {
				String siClass = (siType.substring(2));
				TraffSupplementaryInfo.Class tmpSiClass = null;
				while ((tmpSiClass == null) && !siClass.isEmpty()) {
					try {
						tmpSiClass = TraffSupplementaryInfo.Class.valueOf(siClass);
					} catch (IllegalArgumentException|NullPointerException e) {
						if (siClass.contains("_"))
							siClass = siClass.substring(0, siClass.lastIndexOf('_'));
						else
							siClass = "";
					}
				}
				this.siClass = tmpSiClass;
			}

			TraffSupplementaryInfo.Type tmpSiType;
			try {
				tmpSiType = TraffSupplementaryInfo.Type.valueOf(siType);
			} catch (IllegalArgumentException|NullPointerException e) {
				if ((siType != null) && !siType.isEmpty())
					LOG.warn("Cannot create mapping for unknown SI type {}", siType);
				tmpSiType = null;
			}
			this.siType = tmpSiType;
		}
	}

	/**
	 * A comparator for strings containing numbers.
	 *
	 * <p>To compare two strings, each string is broken down into numeric and non-numeric parts.
	 * Then both string are compared part by part. The following rules apply:
	 * <ul>
	 * <li>Whitespace bordering on a number is discarded</li>
	 * <li>Whitespace surrounded by string characters is treated as one string with the surrounding
	 * characters</li>
	 * <li>If one string has more parts than the other, the shorter string is padded with null
	 * parts.</li>
	 * <li>null is less than non-null.</li>
	 * <li>null equals null.</li>
	 * <li>A numeric part is less than a string part.</li>
	 * <li>Numeric parts are compared as integers.</li>
	 * <li>String parts are compared as strings.</li>
	 * </ul>
	 * 
	 * <p>If one of the strings supplied is null or empty (but not the other), it is considered to be
	 * greater than the other, causing it to be inserted at the end.
	 */
	public static class NumberStringComparator implements Comparator<String> {
		private enum State { WHITESPACE, NUMERIC, ALPHA };

		/**
		 * @return -1 if {@code lhs} appears first as per the sort order, 1 if {@code rhs} appears
		 * first, 0 if no order can be determined 
		 */
		public int compare(String lhs, String rhs) {
			int res = 0;
			if ((lhs == null) || lhs.isEmpty()) {
				if ((rhs == null) || rhs.isEmpty())
					return 0;
				else
					return 1;
			} else if ((rhs == null) || rhs.isEmpty())
				return -1;

			int i;
			List<Object> l = parse(lhs);
			List<Object> r = parse(rhs);
			for (i = 0; i < Math.min(l.size(), r.size()); i++) {
				if (l.get(i) instanceof Integer) {
					if (r.get(i) instanceof Integer)
						res = (Integer) l.get(i) - (Integer) r.get(i);
					else if (r.get(i) instanceof String)
						return -1;
				} else if (l.get(i) instanceof String) {
					if (r.get(i) instanceof Integer)
						return 1;
					else if (r.get(i) instanceof String)
						res = ((String) l.get(i)).compareTo((String) r.get(i));
				}
				if (res != 0)
					return res;
			}

			if (i < l.size())
				return 1;
			else if (i < r.size())
				return -1;
			else
				return 0;
		}

		/**
		 * Parses a string into numeric and non-numeric components.
		 * @param string The string to parse
		 * @return A list of {@link String} and {@link Integer} objects.
		 */
		private List<Object> parse(String string) {
			List<Object> res = new LinkedList<Object>();
			State state = State.WHITESPACE;
			int i = 0;

			while (i < string.length()) {
				char c = string.charAt(i);
				if (c <= 0x20) {
					/* whitespace */
					if (state == State.NUMERIC) {
						res.add(Integer.valueOf(string.substring(0, i)));
						string = string.substring(i);
						i = 1;
						state = State.WHITESPACE;
					} else
						i++;
				} else if ((c >= '0') && (c <= '9')) {
					/* numeric */
					if (state == State.ALPHA) {
						res.add(string.substring(0, i).trim());
						string = string.substring(i);
						i = 1;
					} else
						i++;
					state = State.NUMERIC;
				} else {
					/* alpha */
					if (state == State.NUMERIC) {
						res.add(Integer.valueOf(string.substring(0, i)));
						string = string.substring(i);
						i = 1;
					} else
						i++;
					state = State.ALPHA;
				}
			}

			if (string.length() > 0) {
				if (state == State.NUMERIC)
					res.add(Integer.valueOf(string));
				else if (state == State.ALPHA)
					res.add(string.trim());
			}

			return res;
		}
	}
}
