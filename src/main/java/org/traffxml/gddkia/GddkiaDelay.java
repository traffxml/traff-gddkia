/*
 * Copyright © 2019 traffxml.org.
 * 
 * This file is part of the traffxml-gddkia library.
 *
 * The library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the library.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.traffxml.gddkia;

import java.time.Duration;
import java.time.LocalTime;

/**
 * Encapsulates a delay in GDDKiA format, equivalent to a {@code czas_oczekiwania} element in a feed.
 *
 * New delays are generated using a {@link Builder}.
 */
public class GddkiaDelay {
	public final String kierunek;
	public final LocalTime czasOd;
	public final LocalTime czasDo;

	/**
	 * Delay on weekdays.
	 */
	public final Duration pnPt;

	/**
	 * Delay on Saturdays and Christmas Eve.
	 */
	public final Duration so;

	/**
	 * Delay on Sundays and public holidays.
	 */
	public final Duration ni;

	private GddkiaDelay(String kierunek, LocalTime czasOd, LocalTime czasDo, Duration pnPt, Duration so,
			Duration ni) {
		super();
		this.kierunek = kierunek;
		this.czasOd = czasOd;
		this.czasDo = czasDo;
		this.pnPt = pnPt;
		this.so = so;
		this.ni = ni;
	}

	public static class Builder {
		String kierunek = null;
		LocalTime czasOd = null;
		LocalTime czasDo = null;
		Duration pnPt = null;
		Duration so = null;
		Duration ni = null;

		public GddkiaDelay build() {
			return new GddkiaDelay(kierunek, czasOd, czasDo, pnPt, so, ni);
		}

		public void setKierunek(String kierunek) {
			this.kierunek = kierunek;
		}

		public void setCzasOd(LocalTime czasOd) {
			this.czasOd = czasOd;
		}

		public void setCzasDo(LocalTime czasDo) {
			this.czasDo = czasDo;
		}

		public void setPnPt(Duration pnPt) {
			this.pnPt = pnPt;
		}

		public void setSo(Duration so) {
			this.so = so;
		}

		public void setNi(Duration ni) {
			this.ni = ni;
		}
	}
}
