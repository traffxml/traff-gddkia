package org.traffxml.test.gddkia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.traffxml.gddkia.GddkiaFeed;
import org.traffxml.gddkia.GddkiaMessage;
import org.traffxml.gddkia.GddkiaMessage.Direction;
import org.traffxml.gddkia.GddkiaMessage.Point;
import org.traffxml.lib.milestone.Junction;
import org.traffxml.traff.TraffEvent;
import org.traffxml.traff.TraffFeed;
import org.traffxml.traff.TraffLocation.Point.Role;
import org.traffxml.viewer.TraffViewer;
import org.traffxml.viewer.input.ConverterSource;
import org.traffxml.viewer.input.StaticSource;
import org.traffxml.traff.TraffSupplementaryInfo;

public class GddkiaTest {
	private static final GddkiaConverter CONVERTER = new GddkiaConverter();
	private static final String DEFAULT_URL = "https://www.gddkia.gov.pl/dane/zima_html/utrdane.xml";
	private static final int FLAG_CONVERT = 0x1;
	private static final int FLAG_DISSECT = 0x2;
	private static final int FLAG_GUI = 0x10000;
	private static final int MASK_OPERATIONS = 0xFFFF;

	private static String getParam(String param, String[] args, int pos) {
		if(pos >= args.length) {
			System.out.println("-" + param + " needs an argument.");
			System.exit(1);
		}
		return args[pos];
	}

	private static void printUsage() {
		System.out.println("Arguments:");
		System.out.println("  -convert <feed>  Convert <feed> to TraFF");
		System.out.println("  -dissect <feed>  Examine <feed> and print results");
		System.out.println("  -gui             Launch TraFF Viewer UI (not with -dissect)");
		System.out.println();
		System.out.println("<feed> can refer to a local file or an http/https URL");
	}

	public static void main(String[] args) {
		GddkiaFeed feed = null;
		String input = null;
		int flags = 0;

		if (args.length == 0) {
			printUsage();
			System.exit(1);
		}
		for (int i = 0; i < args.length; i++) {
			if ("-convert".equals(args[i])) {
				input = getParam("convert", args, ++i);
				flags |= FLAG_CONVERT;
			} else if ("-dissect".equals(args[i])) {
				input = getParam("dissect", args, ++i);
				flags |= FLAG_DISSECT;
			} else if ("-gui".equals(args[i])) {
				flags |= FLAG_GUI;
			} else {
				System.out.println("Unknown argument: " + args[i]);
				System.out.println();
				printUsage();
				System.exit(0);
			}
		}
		if (flags == 0) {
			System.err.println("A valid option must be specified.");
			System.exit(1);
		}
		if (flags == FLAG_GUI) {
			TraffViewer.launch(CONVERTER, DEFAULT_URL);
		} else {
			if (input.startsWith("http://") || input.startsWith("https://")) {
				URL url;
				try {
					url = new URL(input);
					feed = CONVERTER.parse(url);
				} catch (MalformedURLException e) {
					System.err.println("Input URL is invalid. Aborting.");
					System.exit(1);
				} catch (IOException e) {
					System.err.println("Cannot read from input URL. Aborting.");
					System.exit(1);
				}
			} else {
				File inputFile = new File(input);
				try {
					feed = CONVERTER.parse(inputFile);
				} catch (FileNotFoundException e) {
					System.err.println("Input file does not exist. Aborting.");
					System.exit(1);
				} catch (IllegalArgumentException e) {
					System.err.println(String.format("%s. Aborting.", e.getMessage()));
					System.exit(1);
				}
			}
			if (feed != null) {
				switch(flags & MASK_OPERATIONS) {
				case FLAG_CONVERT:
					convertFeed(feed, (flags & FLAG_GUI) != 0);
					break;
				case FLAG_DISSECT:
					dissectFeed(feed);
					break;
				default:
					System.err.println("Invalid combination of options. Aborting.");
					System.exit(1);
				}
			} else {
				System.err.println("Feed is null. Aborting.");
				System.exit(1);
			}
		}
	}

	static void convertFeed(GddkiaFeed feed, boolean gui) {
		try {
			TraffFeed tFeed = CONVERTER.convert(feed);
			if (gui) {
				StaticSource.readFeed(tFeed);
				TraffViewer.launch(CONVERTER, DEFAULT_URL);
			} else
				System.out.println(tFeed.toXml());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static void dissectFeed(GddkiaFeed feed) {
		int directionalityCount[] = {0, 0, 0};
		int dirCount[] = {0, 0, 0};
		int endpointCount[] = {0, 0, 0};
		int fallbackEventCount = 0;
		for (GddkiaMessage message : feed.utr) {
			dissectMessage(message);
			Boolean directionality = message.extractDirectionality();
			if (directionality == null)
				directionalityCount[0]++;
			else if (!directionality)
				directionalityCount[2]++;
			else if (directionality) {
				directionalityCount[1]++;
				Direction direction = message.extractDirection();
				if (direction == null)
					dirCount[0]++;
				else if (direction == Direction.FORWARD)
					dirCount[1]++;
				else if (direction == Direction.BACKWARD)
					dirCount[2]++;
			}
			Map<Role, Point> location = message.extractEndpoints();
			endpointCount[(location == null) ? 0 : location.containsKey(Role.AT) ? 1 : 2]++;
			List<TraffEvent> events = message.extractEvents();
			if (events.isEmpty() || ((events.size() == 1)
					&& (events.get(0).type == TraffEvent.Type.CONGESTION_TRAFFIC_PROBLEM)))
				fallbackEventCount++;
		}
		System.out.println(String.format("Messages in feed: %d", feed.utr.length));
		System.out.println(String.format("  Unidirectional: %d", directionalityCount[1]));
		System.out.println(String.format("    Forward:  %d", dirCount[1]));
		System.out.println(String.format("    Backward: %d", dirCount[2]));
		System.out.println(String.format("    Unknown:  %d", dirCount[0]));
		System.out.println(String.format("  Bidirectional:  %d", directionalityCount[2]));
		System.out.println(String.format("  Unknown:        %d", directionalityCount[0]));
		System.out.println(String.format("  Location"));
		System.out.println(String.format("    Double Point: %d", endpointCount[2]));
		System.out.println(String.format("    Single Point: %d", endpointCount[1]));
		System.out.println(String.format("    Unknown:      %d", endpointCount[0]));
		System.out.println(String.format("  Without events: %d", fallbackEventCount));
	}

	static void dissectMessage(GddkiaMessage message) {
		System.out.println(String.format("Message: %s@%.3f+%.3f",
				message.nrDrogi, message.km, message.dl));
		GddkiaMessage.Territory territory = message.extractTerritory();
		System.out.println(String.format("  Territory: %s (%s)", territory != null ? territory.ref : "null", message.woj));
		Boolean isUnidirectional = message.extractDirectionality();
		System.out.println(String.format("  Unidirectional: %s", isUnidirectional));
		if ((isUnidirectional != null) && isUnidirectional) {
			Direction direction = message.extractDirection();
			System.out.println(String.format("  Direction: %s", direction));
		}
		Map<Role, Point> location = message.extractEndpoints();
		if (location == null) {
			if ((message.geoLat == null) || (message.geoLong == null))
				System.out.println("  Location: null");
			else
				System.out.println(String.format("  Location: null, see https://www.openstreetmap.org/?mlat=%f&mlon=%f",
						message.geoLat, message.geoLong));
		} else {
			System.out.println("  Location:");
			Junction[] endpointJunctions = message.extractEndpointJunctions();
			for (Role role : location.keySet()) {
				Junction endpointJunction = (role == Role.FROM) ? endpointJunctions[0] :
					(role == Role.TO) ? endpointJunctions[1] : null;
				String endpointJunctionString;
				/* TODO ref */
				if (endpointJunction != null)
					if (endpointJunction.ref == null)
						endpointJunctionString = " – " + endpointJunction.name;
					else
						endpointJunctionString = " – " + endpointJunction.name + " (" + endpointJunction.ref + ")";
				else
					endpointJunctionString = "";
				System.out.println(String.format("    %-7s %.5f, %.5f%s", role.name(),
						location.get(role).geo.lat, location.get(role).geo.lon, endpointJunctionString));
			}
		}
		System.out.println(String.format("  Destination: %s", message.extractDestination()));
		String[] junctions = message.extractJunctions();
		System.out.println(String.format("  Junctions: %s, %s", junctions[0], junctions[1]));
		Junction[] enclosingJunctions = message.extractEnclosingJunctions();
		System.out.println(String.format("             %s → %s",
				enclosingJunctions[0] == null ? null : enclosingJunctions[0].name,
				enclosingJunctions[1] == null ? null : enclosingJunctions[1].name));
		List<TraffEvent> events = message.extractEvents();
		System.out.println("  Events:");
		if (events.isEmpty())
			System.out.println("    (empty)");
		else
			for (TraffEvent event : events) {
				System.out.println(String.format("    %s", event.type));
				if (event.speed != null)
					System.out.println(String.format("      Speed: %d", event.speed));
				if (event.quantifier != null)
					System.out.println(String.format("      %s", event.quantifier.toAttribute()));
				for (TraffSupplementaryInfo si : event.supplementaryInfos) {
					System.out.println(String.format("      %s", si.type));
					if (si.quantifier != null)
						System.out.println(String.format("        %s", si.quantifier.toAttribute()));
				}
			}
		System.out.println("");
	}

	private static class GddkiaConverter implements ConverterSource {

		@Override
		public TraffFeed convert(File file) throws Exception {
			GddkiaFeed feed = parse(file);
			return convert(feed);
		}

		@Override
		public TraffFeed convert(URL url) throws Exception {
			GddkiaFeed feed = parse(url);
			return convert(feed);
		}

		@Override
		public TraffFeed convert(Properties properties) throws Exception {
			throw new UnsupportedOperationException("This operation is not implemented yet");
		}

		private TraffFeed convert(GddkiaFeed feed) {
			return new TraffFeed(feed.toTraff("test"));
		}

		private GddkiaFeed parse(File file) throws FileNotFoundException {
			if (!file.exists()) {
				throw new IllegalArgumentException("Input file does not exist");
			} else if (!file.isFile()) {
				throw new IllegalArgumentException("Input file is not a file");
			} else if (!file.canRead()) {
				throw new IllegalArgumentException("Input file is not readable");
			}
			return GddkiaFeed.parseXml(new FileInputStream(file));
		}

		private GddkiaFeed parse(URL url) throws IOException {
			return GddkiaFeed.parseXml(url.openStream());
		}
	}
}
